"""
    Read-only parser for raw iTunes Library database file
    for getting track, album, artist and artwork information
    (c) 2012, LAI <info at iscla net>
"""

__author__  = "LAI <info at iscla net>"
__credits__ = "AlfredJKwack, Anoop Menon, jingai, the pycrypto team"
__url__     = "git://github.com/AlfredJKwack/plugin.audio.itunes.git"


import traceback
import xml.parsers.expat
import sys
import os
import os.path
import locale
#sys.path.append("./PIL")
#from urllib import unquote
from struct import *
from utilFunctions import *
from Crypto.Cipher import AES
from ITLBlock import *
from SyncTunesDB import *
import zlib
from operator import mul

#from ItcImagePlugin import itcToPictureFile

global albumMapping, albumFromTrackMapping, artistMapping, albumArtistMapping, albumArtistFromAlbumMapping, trackMapping, composerMapping
global stdb


# mapping iTunes field names into SyncTunes field names
# the order is important: lowest priority should appear first to enable overriding by any redundant field
# e.g. [('artistshortername', 'sortname'), ('sortartist', 'sortname')] means sortartist prevails over 'artistshortername'

albumMapping = OrderedDict([('genericname', 'name'), ('albumname', 'name'), ('compilation', 'compilation'), ('albumRating', 'rating'), ('persistentID', 'persistent'), ('artistid', 'artistid')]) # , ('entryID', 'id')

albumFromTrackMapping = OrderedDict([('genericname', 'name'), ('albumname', 'name'), ('compilation', 'compilation'), ('albumRating', 'rating'), ('persistentID', 'trackpersistent'), ('filename','filename')])  # , ('entryID', 'id')])    , ('albumartistid', 'artistid')

albumArtistFromAlbumMapping = OrderedDict([('artistname3', 'name'), ('albumartist', 'name')]) #, ('artistID', 'id')

artistMapping = OrderedDict([('artist', 'name'), ('albumartistid', 'id')]) #, ('artistshortername', 'sortname'), ('sortartist', 'sortname')]) #, ('artistID', 'id')])

albumArtistMapping = OrderedDict([('albumartist', 'name')]) #, ('sortalbumartist', 'sortname')]) #, ('artistID', 'id')

trackMapping = OrderedDict([('genericname', 'title'), ('trackname', 'title'), ('trackNumber', 'tracknumber'), ('diskNumber', 'disknumber'), ('year', 'year'), ('filename', 'filename'), ('entryID', 'id'), ('rating', 'rating'),  ('persistentID', 'persistent'), ('playCount', 'playcount'), ('genreid', 'genreid'), ('artistid', 'artistid'), ('composerid', 'composerid'), ('albumid', 'albumid'), ('playTime', 'duration'), ('artworkpath', 'artworkpath')]) # ('albumID', 'albumid'),

composerMapping = OrderedDict([('composer', 'name')]) #, ('sortcomposer', 'sortname')])

genreMapping = OrderedDict([('genre', 'name')])

global iTunesLibraryName



def dbUpload(recordType, recordAttributes):
    #debug_dump("recordType: %s" % recordType)
    if not recordType in ['haim', 'hpim', 'htim', 'hghm', 'hdfm', 'hdsm']:
        return
    global albumMapping, albumFromTrackMapping, artistMapping, albumArtistFromAlbumMapping, albumArtistMapping, trackMapping, composerMapping
    global stdb
    #debug_dump("Entering Block.dbUpload()", 1)
    try:
        if recordType == 'haim':  # album and apps
            artist = stdb.findOrCreateArtist(map4SyncTunesDB(recordAttributes, albumArtistFromAlbumMapping), 'albumartist')
            recordAttributes.update({'artistid': artist['id']})
            stdb.findOrCreateAlbum(map4SyncTunesDB(recordAttributes, albumMapping))
        elif recordType == 'htim': # track
            if recordAttributes['category'] == 5 or recordAttributes['category'] == 4:
                artist = stdb.findOrCreateArtist(map4SyncTunesDB(recordAttributes, artistMapping), 'artist')
                recordAttributes.update({'artistid': artist['id']})

                composer = stdb.findOrCreateArtist(map4SyncTunesDB(recordAttributes, composerMapping), 'composer')
                recordAttributes.update({'composerid': composer['id']})
            
                #Already (theoretically) declared when parsing haim
                #albumArtist = stdb.findOrCreateArtist(map4SyncTunesDB(recordAttributes, albumArtistMapping), 'albumartist')
                #recordAttributes.update({'albumartistid': albumArtist['id']})
            
                genre = stdb.findOrCreateGenre(map4SyncTunesDB(recordAttributes,genreMapping))
                recordAttributes.update({'genreid': genre['id']})
            
                album = stdb.findOrCreateAlbum(map4SyncTunesDB(recordAttributes, albumFromTrackMapping), False)
                recordAttributes.update({'albumid': album['id']})
                try:
                    recordAttributes.update({'artworkpath': album['artworkpath']})
                #debug_dump("Album artwork at %s" % album['artworkpath'])
                except:
                    pass
                stdb.updateOrCreateTrack(map4SyncTunesDB(recordAttributes, trackMapping))
        elif recordType == 'hghm': # general header
            iTunesLibraryName = recordAttributes['libraryName']
            stdb.setConfigItem("Library Name", iTunesLibraryName)
        elif recordType == 'hdfm': # general header
            stdb.setConfigItem("Library Persistent ID", recordAttributes['persistentID'])
        elif recordType == 'hpim': # playlist
            pass
        elif recordType == 'hdsm':
            if 'musicDirectoryPath' in recordAttributes:
                stdb.setConfigItem("Music directory", recordAttributes['musicDirectoryPath'])
    except Exception, e:
        debug_dump(to_str(e))
        debug_dump(traceback.format_exc())
        raise e
    except:
        debug_dump(traceback.format_exc())
        raise
    #finally:
        #debug_dump("Returning from Block.dbUpload()", -1)


def map4SyncTunesDB(attributes, mappingTable):
    #debug_dump("Entering Block.map4SyncTunesDB(%s, %s)" % (attributes if attributes <> None else "<Null>", mappingTable if mappingTable <> None else "<Null>"), 1)
    try:
        # below not working with OrderedDicts ?
        #result = dict({[(mappingTable[x], attributes[x]) for x in filter(attributes.has_key, mappingTable.keys())]})
        result = {}
        for key in mappingTable.iterkeys():
            if key in attributes:
                result[mappingTable[key]] = attributes[key]
        return result
    except:
        debug_dump(traceback.format_exc())
        raise
    #finally:
#    debug_dump("Returning from Block.map4SyncTunesDB(%s)" % result, -1)


class ITLFile:
    #    MIN_BUFFSZ = 98304
    MIN_BUFFSZ = 524288
    Key = "BHUILuilfghuila3"
    
    def __init__(self, addon=None, progressCallback=None):
        #debug_dump("Entering ITLFile.__init__()", 1)
        self.iTunesVersion = ""
        self.dumpClearFile = ""
        self.progressFactors = {}
        self.progress = 0
        self.progressCallback = progressCallback
        self.addon = addon
        #debug_dump("Returning from ITLFile.__init__()", -1)
    
    def open(self, fileName, uploadCallback = None):
#        debug_dump("Entering ITLFile.open(%s)" % (fileName), 1)
        try:
            if not os.path.exists(fileName) or not os.path.isfile(fileName):
                raise Exception ("File not found: " + fileName + ".")
                return 1
            self.ITLFilePath = fileName
            self.file = open(self.ITLFilePath, "rb")
            self.size = os.path.getsize(fileName)
            self.iTunesDir = os.path.dirname(fileName)
            self.currentOffset = 0
            self.buffer = ""
            initBlock = Block(self, uploadCallback)
            self.iTunesVersion = initBlock.attr['version']
#        except Exception, e:
#            debug_dump(to_str(e))
#            raise e        
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise
#        debug_dump("Returning from ITLFile.open(). iTunes Version: %s" % self.iTunesVersion, -1)

                
    def setDumpFile(self, fileName):
        #debug_dump("Setting output file for dump...")
        self.dumpFile = open(fileName, "wb")

                
    def __readFile(self,size):
        try:
            try:
                self.totalRead
            except:
                self.totalRead = 0
            if size <= 0:
                raise Exception ("Size for reading file should be above 0")
            #debug_dump("ITLFile.__readFile(%d). File offset: %d, vOffset: %d" % (size, self.__fileTell(), self.currentOffset))
            result = self.file.read(size)
            self.totalRead += len(result)
            #debug_dump("_readFile read so far: %d out of %d" % (self.totalRead, self.size))
            self.__updateProgress('physicalRead', float(self.__fileTell()) / float(self.size))
            return result
        except UserCancel:
            debug_dump("User cancelled")
            raise
        except SystemExit:
            raise
        except:
            debug_dump(traceback.format_exc())
            raise

    def __updateProgress(self, factorName, value):
        try:
            self.progressFactors.update({factorName: value})
            newProgress = int(round(100*reduce(mul, self.progressFactors.values()),0))
            if (factorName == 'parsed' and newProgress > self.progress):
                debug_dump("Import progress: %d%%" % newProgress)
                self.progress = newProgress
                try:
                    self.progressCallback(newProgress, 100)
                except (UserCancel, SystemExit):
                    raise
                except:
                    pass
            else:
                try:
                    self.progressCallback() # we want to catch any user cancellation asap
                except (UserCancel, SystemExit):
                    raise
                except:
                    pass
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise
        
    def complete(self):
        return (self.__fileTell() >= self.size and self.buffer == "")


    def read(self, size):
        #debug_dump("Entering ITLFile.read(%d). Buffer size: %d, vOffset: %d" % (size, len(self.buffer), self.currentOffset), 1)
        #debug_dump("Entering ITLFile.read(%d). Buffer size: %d, fileTell: %d" % (size, len(self.buffer), self.__fileTell()), 0)
        try:
            if size <= 0:
                raise Exception ("Read size should be above 0")
            if (self.iTunesVersion == ""):  ## happens for the very first (initialization) hdm block
                result = self.__readFile(size)
                self.inflatedSize = len(result)
                self.currentOffset += len(result)
                return result
            oriLen = len(self.buffer)
            self.__inflate(size-len(self.buffer))
            self.inflatedSize += len(self.buffer) - oriLen
            if len(self.buffer) < size:
                if not self.complete():
                    raise Exception ("Cannot inflate file sufficiently to read %d bytes. Buflen: %d. Bufempty: %d. fileTell: %d. size: %d. complete: %d." %(size, len(self.buffer), self.buffer == "", self.__fileTell(), self.size, self.complete()))
                else:
                    raise BufferError ("File end reached smartly with some uninterpreted bytes. Ignoring.")

            result = self.buffer[:size]
            self.buffer = self.buffer[size:]
            self.currentOffset += len(result)
            self.__updateProgress("parsed", float(self.currentOffset)/float(self.inflatedSize))
            try:
                self.dumpFile.write(result)
            except:
                pass

            return result
        except (UserCancel, SystemExit):
            raise
        except BufferError, e:
            debug_dump(to_str(e))
            pass
        except Exception, e:
            debug_dump( to_str(e))
            #pass
            raise e
        except:
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from ITLFile.read(). Buf len: %d, vOffset: %d" % (len(self.buffer), self.currentOffset), -1)


    def __fileTell(self):
        return self.file.tell()

    def seek(self, size):
        try:
            self.read(size)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise

    def tell(self):
        return self.currentOffset

    def close(self):
        try:
            self.file.close()
        except:
            pass
        try:
            self.dumpFile.close()
            debug_dump("File dumped")
        except:
            pass




    def __decrypt(self, size):
        try:
            result = ""
            #debug_dump("Entering ITLFile.__decrypt(%d)" %size,1)
            if size < 0:
                return ""
            try:
                self.decryptInitialized
            except AttributeError:
                self.decryptInitialized = True
                self.decryptedBuffer = ""
                self.decryptSize = 0
                encryptedLength = self.size - self.file.tell()
                self.encryptionStart = self.currentOffset
                if int(self.iTunesVersion.partition(".")[0])>= 10:
                    # need to decrypt
                    self.encryptedLength = min(encryptedLength, 102400);
                    self.encryptedLength -= self.encryptedLength % 16;
                else:
                    self.encryptedLength = 0
                #debug_dump("Encrypted length: %d" %self.encryptedLength)
                pass
            try:
                result = self.decryptedBuffer[:size]
                self.decryptedBuffer = self.decryptedBuffer[size:]
                if len(result) < size:
                    if self.__fileTell() < self.encryptionStart + self.encryptedLength:
                        cipher = AES.new(self.Key, AES.MODE_ECB)
                        toDecrypt = self.__readFile(self.encryptedLength)  # need to change this to deal with trunks if encrypted length increases
                        self.decryptedBuffer += cipher.decrypt(toDecrypt)
                        fillSize = size-len(result)
                        result += self.decryptedBuffer[:fillSize]
                        self.decryptedBuffer = self.decryptedBuffer[fillSize:]
                        #debug_dump("Decrypted buffer size: %d / Returned buffer size: %d" % (len(self.decryptedBuffer),len(result)))
                    else:
                        #debug_dump("No further decrypt required.")
                        result += self.__readFile(size-len(result))
                #debug_dump("Returning from ITLFile.__decrypt() size: %d" % len(result),-1)
                self.decryptSize += len(result)
                self.__updateProgress("decryptBuffer", float(self.decryptSize) / float(self.__fileTell()))
                return result
            except Exception, e:
                debug_dump(to_str(e))
                #pass
                raise e
        except:
            debug_dump(traceback.format_exc())
            raise
 
                
    def __inflate(self, size):
        if size < 0: return ""
        try:
            #debug_dump("ITLFileParser.__inflate(%d). File offset: %d, vOffset: %d" % (size, self.__fileTell(), self.currentOffset),1)
            self.probablyCompressed
        except AttributeError:
            # this is done only once, at the start of the compressed section
            self.zobj = zlib.decompressobj()
            self.inflowQty = 0
            possiblyCompressedBuffer = self.__decrypt(self.MIN_BUFFSZ)
            self.inflowQty += len(possiblyCompressedBuffer)
            self.probablyCompressed = (self.size - self.currentOffset >= 1 and possiblyCompressedBuffer[0] == '\x78') # ' 0x78 seems to be the first char of an archive
            # debug_dump("probablyCompressed? %d" % self.probablyCompressed)
            if self.probablyCompressed:
                self.compressedBuffer = possiblyCompressedBuffer
            else:
                self.buffer += possiblyCompressedBuffer
                self.compressedBuffer = ""
                if len(self.buffer) > size: return
        try:
            oriSize = len(self.buffer)
            if self.probablyCompressed:
                oriCompressedSize = len(self.compressedBuffer)
                if oriCompressedSize*3 > size: # try to find the quantity in the compressed buffer
                    self.buffer += self.zobj.decompress(self.compressedBuffer, max(size,self.MIN_BUFFSZ))
                    self.compressedBuffer = self.zobj.unconsumed_tail
                if len(self.buffer) < (oriSize + size): # we need to read/decrypt more
                    oriCompressedSize = len(self.compressedBuffer)
                    self.compressedBuffer += self.__decrypt(max(size-len(self.buffer), self.MIN_BUFFSZ))
                    self.inflowQty += len(self.compressedBuffer) - oriCompressedSize
                    saveTail = self.compressedBuffer
                    self.buffer += self.zobj.decompress(self.compressedBuffer, max(size, self.MIN_BUFFSZ))
                    self.compressedBuffer = self.zobj.unconsumed_tail
                    if (self.buffer == "" and self.compressedBuffer == ""):
                        # we're at the end of the file, finish the dumping as appropriate
                        # by the way, normally the first byte of this tail should also be "\x78"
                        self.dumpFile.write(saveTail)

            #updateIndex = (theSize - len(self.compressedBuffer))/len(self.compressedBuffer)
                #debug_dump("Result size: %d / Leftover len: %d" % (len(self.buffer)-oriSize, len(self.compressedBuffer)))
            else:
                self.buffer += self.__decrypt(size)
                self.inflowQty += len(self.buffer) - oriSize
            self.__updateProgress("inflateBuffer", float(self.inflowQty)/float(self.inflowQty+len(self.compressedBuffer)))
        except:
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from ITLFileParser.__inflate()",-1)



class ITLFileParser:
    
    def __init__(self, itlFilePath, synctunesDB, addon = None, progressCallback = None):
        global stdb
        try:
            self.addon = addon
            self.progressCallback = progressCallback
            stdb = synctunesDB
            self.DB = synctunesDB
#            self.tempDB = os.path.splitext(synctunesDB)[0] + "_NEW" + os.path.splitext(synctunesDB)[1]
            self.itlFilePath = itlFilePath
            self.file = ITLFile(addon, progressCallback)

        except:
            debug_dump(traceback.format_exc())
            raise


    def doParse(self):
        global theDB
        try:
#            debug_dump("Entering doParse()",1)
            #self.file.setDumpFile("/Users/arnaud/Desktop/toto5.itl")
            self.file.open(self.itlFilePath, dbUpload)
            
            self.DB.prepareForUpdate()
            while not self.file.complete():
                theBlock = Block(self.file, dbUpload)

            self.DB.commit()
#            debug_dump("Returning from doParse()",-1)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise
        finally:
            self.file.close()
#os.remove(self.tempDB)


progressCnt=0

def __test_progressCallback(a=0, b=0):
    global progressCnt
    global testUserCancel
    
    progressCnt += 1
    try:
        if progressCnt >= 20 and testUserCancel:
            raise UserCancel
    except UserCancel:
        print("User cancel faked... NOW")
        raise

def __test__():
    global debug_flag, stdb, testUserCancel
    testUserCancel = False # change here if you want to test propagation of user cancellation
    #debug_flag = True
    debug_dump("Testing ----",1)
    
    try:
        doInit = True if '--reset' in sys.argv else False

        #theLibFile = "/Volumes/Rallonge/Shared/iTunes/iTunes Library"
        theLibFile = "/Users/arnaud/Music/iTunes.local/iTunes Library.itl"
        theDB = SyncTunesDB("/Users/arnaud/Desktop/stdb.db", theLibFile, None, doInit)
        theLib = ITLFileParser(theLibFile, theDB) #, progressCallback = __test_progressCallback)
        
        #we will dump the decrypted+decompressed library (on the fly)
        theLib.file.setDumpFile("/Users/arnaud/Desktop/test6.itl")
        
        theLib.doParse()
        debug_dump ("Testing complete ---",-1)
    except UserCancel:
        debug_dump("User Cancellation received. Terminated.")

def main():
    if '--help' in sys.argv or '-help' in sys.argv:
        print ""
        print "Syntax description:"
        print "-------------------"
        print "%s --lib file --db file [--dump file] [--reset]" % (os.path.basename(__file__))
        print "%s --help" % (os.path.basename(__file__))
        print ""
        print "--lib: specifies the path of the iTunes Library file (usually suffixed with .itl)"
        print "--db: specifies the path of the SQLite database"
        print "--dump: specifies a path for a dump of the uncompressed iTunes library file (usually for debug purposes)"
        print "--reset: forces reset (i.e. content drop) of the database before import"

        exit(0)
    if '--ping' in sys.argv:
        exit(0)
            
    if '--test' in sys.argv:
        __test__()
        exit(0)

    try:
        if len(sys.argv) < 4: raise Exception("Missing arguments. Use --help as argument for syntax description")
        theLibFile = sys.argv[sys.argv.index('--lib')+1]
        theDBPath = sys.argv[sys.argv.index('--db')+1]
        if '--dump' in sys.argv:
            theDumpFile = sys.argv[sys.argv.index('--dump')+1]
    except ValueError,v:
        debug_dump(to_str(v))
        exit(1)
    except IndexError, i:
        debug_dump("Missing value for argument %s" % sys.argv[len(sys.argv)-1])
        exit(1)
    except Exception, e:
        debug_dump(to_str(e))
        raise

    try:
        doInit = True if '--reset' in sys.argv else False
        theDB = SyncTunesDB(theDBPath, theLibFile, None, doInit)
        theLib = ITLFileParser(theLibFile, theDB)
        
        #we will dump the decrypted+decompressed library (on the fly)
        try:
            theLib.file.setDumpFile(theDumpFile)
        except:
            pass
        
        theLib.doParse()
    except UserCancel:
        debug_dump("User Cancellation received. Terminated.")
    except:
        debug_dump(traceback.format_exc())
        raise

if __name__=="__main__":
    #profile_main()
    main()

# key principle:
# the Library file structure seems to be ORDERED as explained below
# therefore, album information is loaded BEFORE track information, which is loaded BEFORE playlist information
# conclusion: we can afford following the structure of the library file and upload the DB as the information is read
#
# hdfm w/ size = full (encrypted) file)
# |_ hdsm
# |_ hdfm w/ size = 0
# START OF COMPRESSED SECTION (at least on v.10 and higher)
# |_ hdsm = generic header
# |  |_ hghm = general information (name for library sharing, etc.)
# |  |  |_ hohm = granular information ("field")
# |  |  |_ hohm
# |  |  |_ ...
# |_ hdsm
# |  |_ halm = album list
# |  |  |_ haim = album item
# |  |  |  |_ hohm = granular information ("field")
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ haim
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ ...
# |_ hdsm
# |  |_ hilm = indexing list ? (seems to map "album artists" (or app creators) with their sort/shortened version,
# |  |  |                       e.g. "The Beatles" with "Beatles")
# |  |  |_ hiim = album item
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ hiim
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ etc.
# |_ hdsm
# |  |_ htlm = track list
# |  |  |_ htim = track item
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ htim
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ etc.
# |_ hdsm
# |  |_ hplm = playlist list
# |  |  |_ hpim = playlist item
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |  |_ hptm = playlist track (reference to htim)
# |  |  |  |_ hptm
# |  |  |  |_ ...
# |  |  |_ hpim
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |  |_ hptm = playlist track (reference to htim)
# |  |  |  |_ hptm
# |  |  |  |_ ...
# |  |  |_ etc.
# |  |  |  |_ hohm
# |  |  |  |_ hohm
# |  |  |  |_ ...
# |  |  |_ hptm
# |_ hdsm
# |  |_ hplm = ? not sure why ?
# |_ hdsm type = 0x04 seemingly containing a 0x00-left-padded URI path to the Media directory
# |_ hdsm type = 0x0f
# |  |_ hrlm
# |  |_ hrpm
# END OF FILE
