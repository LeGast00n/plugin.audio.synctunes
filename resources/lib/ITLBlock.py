"""
    Read-only parser for raw iTunes Library database file
    for getting track, album, artist and artwork information
    (c) 2012, LAI <info at iscla net>
    """

# References
# http://search.cpan.org/~bdfoy/Mac-iTunes/doc/file_format.pod
# http://code.google.com/p/picardplugins/wiki/LibraryFile
# http://code.google.com/p/titl/w/list

__author__  = "LAI <info at iscla net>"
__credits__ = "joe.walt...@gmail.com (titl project)"
__url__     = ""


import traceback
from struct import *
from sys import version as pythonVersion
if pythonVersion[:3] > '2.7':
    from collections import OrderedDict
else:
    from OrderedDictBackport import OrderedDict
from utilFunctions import *


def str_decode(text, encodingNum = -1):
    #####debug_dump("Entering str_decode(%s,%d)" % (text,encodingNum))
    try:
        if encodingNum == 0:
            return text.decode('us-ascii');
        elif encodingNum == 1:
            return text.decode('utf_16_be')
        elif encodingNum == 2:
            return text.decode('utf_8')
        elif encodingNum == 3:
            return text.decode('windows-1252')
        elif encodingNum == -1:
            if(len(text) > 1 and ((len(data) % 2) == 0) and (data[0] == '\x00')):
                return text.decode('utf_16_be')
            else:
                return text.decode('iso-8859-1')
        else:
            raise Exception("Unknown encoding type %d for %s" % (encodingNum, text))
        return
    except (UserCancel, SystemExit):
        raise
    except:
        print traceback.print_exc()
        raise



class Block(object):
    #hdfm -- file header
    #hdsm -- part header ?
    #htlm -- master items list header ?
    #htim -- master item record
    #hohm -- data sublock, multi-purpose (in htim, hpim blocks)
    #hplm -- playlist list header ?
    #hpim -- start of playlist block
    #hptm -- item indentifier sub-block for hpim
    
    blockTypes = ['hdfm','hdsm','hpim','hptm','htim','haim','hdfm','hiim','hohm','halm','hilm','hplm','hghm',         'htlm',  'hrlm', 'hrpm', 'hqlm', 'hqim']
    readSize = 0

    
    def __init__(self, theFile, attributeProcessingCallback=None):
        try:
            #debug_dump("Entering Block.__init__(). Offset: %#x" % theFile.currentOffset,1)
            self.startOffset = theFile.tell()
            self.ITLFile = theFile
            self.attr = {}
            self.data = {}
            self.attributeProcessingCallback = attributeProcessingCallback
            try:
                headerLabel = unpack(endianPrefix + "4s I I", self.ITLFile.read(12))
            except (UserCancel, SystemExit):
                raise
            except:
                # we've reached the end of the readable buffer/file, error to be processed at read() time
                return
            self.blockType = headerLabel[0]
            if self.blockTypes.count(self.blockType) == 0:
                print("Offset %#x: unknown block type %s" % (self.startOffset, self.blockType))
                #raise Exception ("Offset %#x: unknown block type %s" % (self.startOffset, self.blockType))
                #return 1
            self.headerLength = headerLabel[1]
            self.contentLength = headerLabel[2]

            #debug_dump("Block %s length: %d, content length: %d" % (self.blockType,self.headerLength,self.contentLength))
            if self.blockType == 'hdfm':
                self.__class__ = hdfmBlock
            elif self.blockType == 'hdsm':
                self.__class__ = hdsmBlock
            elif self.blockType == 'hohm':
                self.__class__ = hohmBlock
            elif self.blockType == 'haim':
                self.__class__ = haimBlock
            elif self.blockType == 'hpim':
                self.__class__ = hpimBlock
            elif self.blockType == 'hptm':
                self.__class__ = hptmBlock
            elif self.blockType == 'htim':
                self.__class__ = htimBlock
            elif self.blockType == 'hiim':
                self.__class__ = hiimBlock
            elif self.blockType == 'hghm':
                self.__class__ = hghmBlock
            elif self.blockType in ('halm', 'hilm', 'hplm'):
                self.__class__ = hxlmBlock
            else:
                if self.blockType[0] <> "h": raise Exception ("Invalid block header: %s / Offset: %d" % (self.blockType, self.startOffset))
                self.ITLFile.seek(self.headerLength-12)
                self.size = self.ITLFile.tell() - self.startOffset
                return
            self.loadFromString()
            self.size = self.ITLFile.tell() - self.startOffset

            if self.attributeProcessingCallback:
                self.attributeProcessingCallback(self.blockType, self.attr)
            return
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            print traceback.print_exc()
            raise e
        except:
            print traceback.print_exc()
            raise
        #finally:
            #debug_dump("Returning from Block.__init__()",-1)


    def loadFromString(self):
        try:
            raise Exception ("method not implemented for this object: loadFromString()")
        except Exception, e:
            print to_str(e)
            raise e


class hdfmBlock(Block):
    #    Byte   Length  Comment
    #    -----------------------
    #        0       4     'hdfm'
    #        4       4     L = header length
    #        8       4     file length ?
    #       12       4     ?
    #       13       1     N = length of version string
    #       14       N     application version string
    #      14+N    35-N    padding with 0x00 ??
    #       52       8     library persistentID
    #       60     L-60    ?

    def loadFromString(self):
        global endianPrefix
        #####debug_dump("Entering hdfmBlock.loadFromString()",1)
        try:
            unpkStr = endianPrefix + "I B"
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.data['hdrUnknown1'] = BlockHeader[0]
            self.data['stringLength'] = BlockHeader[1]
            unpkStr = endianPrefix + "%ds %dp Q %dp" % (self.data['stringLength'], 35-self.data['stringLength'], self.headerLength - 60)
            BlockContent = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.attr['version'] = BlockContent[0]
            self.attr['persistentID'] = "{0:X}".format(BlockContent[2])
            #####debug_dump("Version: %s, library persistent ID: %8x" % (self.attr['version'], self.attr['persistentID']))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #####debug_dump("Returning from hdfmBlock.loadFromString()",-1)


class hghmBlock(Block):
    # Seemingly a unique header preceding a list of hohm sub-blocks
    # Header for record detail
    #     Byte   Length  Comment
    #    -----------------------
    #        0       4     'hghm'
    #        4       4     L = header length
    #        8       4     R = number of hohm sub-blocks
    #       12       2     ??
    #       14      L-14   padding with 0x00 ?
    
    def loadFromString(self):
        global endianPrefix
        #debug_dump("Entering hghmBlock.loadFromString()")
        try:
            self.ITLFile.seek(self.headerLength - 12)
            theoreticalSubBlockType = 'hohm'
            i = 1
            while i <= self.contentLength:
                subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                if subBlock.blockType == theoreticalSubBlockType:
                    self.attr.update(subBlock.attr)
                else:
                    raise Exception ("%s block of index %d expected inside %s, found %s" % (theoreticalSubBlockType, i, self.blockType, subBlock.blockType))
                i += 1
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #else:
            ##debug_dump("Returning from %sBlock.loadFromString()" % self.blockType,-1)

####debug_dump("Returning from %sBlock.loadFromString()" % self.blockType,-1)

class hxlmBlock(Block):
    # Seemingly a list header preceding a list of h?im (? = a, i, or p) sub-blocks
    # Header for record detail
    #     Byte   Length  Comment
    #    -----------------------
    #        0       4     'h?lm'
    #        4       4     L = header length, usually 0x5C for halm and 0x64 of hilm
    #        8       4     R = number of h?im sub-blocks (where ? = a, i, or p)
    #       12       2     ??
    #       14      L-14   padding with 0x00 ?
    
    def loadFromString(self):
        global endianPrefix
        #debug_dump("Entering %sBlock.loadFromString()" % self.blockType,1)
        try:
            self.ITLFile.seek(self.headerLength - 12)
            theoreticalSubBlockType = 'h%cim' % self.blockType[1]
            i = 1
            while i <= self.contentLength:
                subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                if subBlock.blockType == theoreticalSubBlockType:
                    self.attr.update(subBlock.attr)
                else:
                    raise Exception ("%s block of index %d expected inside %s, found %s" % (theoreticalSubBlockType, i, self.blockType, subBlock.blockType))
                i += 1
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #finally:
            #debug_dump("Returning from %sBlock.loadFromString()" % self.blockType,-1)




class hdsmBlock(Block):
    # Header for a section ?
    #   Byte   Length  Comment
    #   -----------------------
    #    0       4     'hdsm'
    #    4       4     L = header length
    #    8       4     R = total content size
    #   12       4     block type ?
    #   16      L-16   ?
    
    def loadFromString(self):
        global endianPrefix
        #####debug_dump("Entering hdsmBlock.loadFromString()",1)
        try:
            unpkStr = endianPrefix + "L %ds" % (self.headerLength-16)
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.data['hdsmType'] = BlockHeader[0]
            self.data['headerData'] = BlockHeader[1]
            #####debug_dump("hdsm block sub type: %#x " % self.data['hdsmType'])
            totalSize = self.headerLength
            if self.data['hdsmType'] == 4:
                self.attr['musicDirectoryPath'] = self.ITLFile.read(self.contentLength - self.headerLength)
            #####debug_dump("Music directory path: " + self.attr['musicDirectoryPath'])
            elif self.data['hdsmType'] == 3:
                # skipping
                totalSize = self.contentLength
                self.ITLFile.seek(self.contentLength-self.headerLength)
            else:
                while totalSize < self.contentLength:
                    subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                    self.attr.update(subBlock.attr)
                    totalSize += subBlock.size
        except (UserCancel, SystemExit):
            raise
        except:
            print "hdsm read error for type %d" % (self.data['hdsmType'])
            print traceback.print_exc()
            raise
        #####debug_dump("Returning from hdsmBlock.loadFromString()",-1)


class hohmBlock(Block):
    # Generic header for a record detail
    #     Byte   Length  Comment
    #    -----------------------
    #        0       4     'hohm'
    #        4       4     L = header length, usually 0x18
    #        8       4     R = total record length
    #       12       4     block type, (1, 2, 3, 4, 5, 6, 0x0b, 0x64)
    #       16       4     index# of this type
    #       20       4     ?
    #       24       4     string formatting type 1=us-ascii, 2=utf-16be, 3=utf-8, 4=windows-1252
    #       28     R-28    block specific data
    
    def loadFromString(self):
        global endianPrefix
        #####debug_dump("Entering hohmBlock.loadFromString()",1)
        try:
            #unpkStr = endianPrefix + "I 3I I I I %ds" % (self.contentLength-40)
            unpkStr = endianPrefix + "2I %ds" % (self.contentLength-20)
            theData = self.ITLFile.read(calcsize(unpkStr))
            BlockHeader = unpack(unpkStr, theData)
            self.data['hohmType'] = BlockHeader[0]
            self.data['unknown1'] = BlockHeader[1]
            self.__setAttributeFromFieldID("{0:#x}".format(self.data['hohmType']), BlockHeader[2])
            #####debug_dump("hohm block sub type: %#x / Markers: %d %d %d / Content: %s" % (self.data['hohmType'], self.data['index'], self.data['unknown1'], self.data['strEncoding'], self.attr.viewitems()))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #####debug_dump("Returning from hohmBlock.loadFromString()",-1)



    def __setAttributeFromFieldID(self, fieldID, recordData):
        global endianPrefix
        #####debug_dump("Entering hohmBlock.__setAttributeFromFieldID(%s)" % fieldID, 1)
        try:
            processingType = 1 # conventional processing of the data
                #mappingTable = {
                #'0x02': 'trackname',     # track title
            #'0x03': 'albumname'     # artist
            #}
            fieldName = ''
            if fieldID == '0x1': # pretty complex one
                #####debug_dump("Met hohm type 1")
                pass
            elif fieldID == '0x2': # Track title
                fieldName = 'trackname'
            elif fieldID == '0x3': # Album title
                fieldName = 'albumname'
            elif fieldID == '0x4': # Artist
                fieldName = 'artist'
            elif fieldID == '0x5': # Genre
                fieldName = 'genre'
            elif fieldID == '0x6': # Kind
                fieldName = 'kind'
            elif fieldID == '0x7': # Kind
                fieldName = 'kind2'
            elif fieldID == '0x8':
                pass
            elif fieldID == '0x9': # iTunes category?
                pass
            elif fieldID == '0xb': # Local path as URL XXX
                fieldName = 'filename'
            elif fieldID == '0xd': # Location
                fieldName = 'filelocation'
            elif fieldID == '0xc': # composer
                fieldName = 'composer'
            elif fieldID == '0xe':
                pass
            elif fieldID == '0x11': # URL to AppStore to view podcast
                pass
            elif fieldID == '0x12': # Subtitle?
                fieldName = 'subtitle'
            elif fieldID == '0x13': # Download URL for podcast item
                fieldName = 'podcastdlurl'
                processingType = 2
            elif fieldID == '0x14':
                pass
            elif fieldID == '0x15': # Chapter information for videos (seems to contain sub-struct)
                pass
            elif fieldID == '0x16': # iTunes summary?
                fieldName = 'summary'
            elif fieldID == '0x17': # iTunes podcast keywords
                fieldName = 'podcastkeywords'
            elif fieldID == '0x18':  #  Show (on 'Video' tab)                   'Series'
                pass
            elif fieldID == '0x19':  #  Episode ID (on 'Video' tab)             'Episode'
                pass
            elif fieldID == '0x1a':  #  ?? Studio/Producer, e.g. "Fox"          --n/a--
                pass
            elif fieldID == '0x1b': # Album artist
                fieldName = 'albumartist'
            elif fieldID == '0x1c':  #  mpaa Rating                             'Content Rating'
                pass
            elif fieldID == '0x1d':  #  ?? DTD for Propertylist                 --n/a--
                pass
            elif fieldID == '0x1e':
                pass
            elif fieldID == '0x1f': # Album for sorting
                fieldName = 'sortalbum'
            elif fieldID == '0x20': # Artist for sorting
                fieldName = 'sortartist'
            elif fieldID == '0x21': # Album artist for sorting
                fieldName = 'sortalbumartist'
            elif fieldID == '0x22': # Composer for sorting
                fieldName = 'sortcomposer'
            elif fieldID == '0x23':  #  Sort-order for show title  'Sort Series'
                pass
            elif fieldID == '0x24': # (seems related to video file type ? + creator?)
                pass
            elif fieldID == '0x25': # Podcast URL for item
                fieldName = 'podcaststurl'
            elif fieldID == '0x2b': # ISRC. Could also be a more generic recording ID?
                fieldName = 'isrc'
            elif fieldID == '0x2d': # A version string?
                pass
            elif fieldID == '0x2e': # Copyright/Published notice
                fieldName = 'copyrightpublishingnotice'
                pass
            elif fieldID == '0x2f':
                pass
            elif fieldID == '0x30': # Data for apps
                pass
            elif fieldID == '0x32': # DRM key files? for apps
                pass
            elif fieldID == '0x34': # "flavor", i.e. apparently string containing #canals:bitrate e.g. "2:256"
                pass
            elif fieldID == '0x36': # cloud artwork token (xml)
                pass
            elif fieldID == '0x38': # redownload params (xml)
                pass
            elif fieldID == '0x64': # (Smart?) Playlist title
                fieldName = 'playlistname'
            elif fieldID == '0x65': # Smart criteria
                pass
            elif fieldID == '0x66': # Smart info
                pass
            elif fieldID == '0x67': # Podcast info?
                #fieldName = 'podcastinfo'  # this one messes up as it does not follow the Len byte + padding standard
                # there seems to be also a sub-structure therein - skipping (for now)
                #processingType = 2
                pass
            elif fieldID == '0x68':
                processingType = 2
                pass
            elif fieldID == '0x69':
                pass
            elif fieldID == '0x6a': # A list of bands?
                pass
            elif fieldID == '0x6b':
                pass
            elif fieldID == '0x6c':
                pass
            elif fieldID == '0xc8': # Podcast episode list title
                pass
            elif fieldID == '0xc9': # Podcast title
                fieldName = 'podacasttitle'
                pass
            elif fieldID == '0x12c': # General title (XXX not just podcasts)
                fieldName = 'genericname'
            elif fieldID == '0x12d': # Album artist
                fieldName = 'albumartist'
            elif fieldID == '0x12e': # Artist again? Album artist?
                fieldName = 'artistname3'
            elif fieldID == '0x130': #  ??  Show/Series: I think it's used for
                pass
            elif fieldID == '0x131': # Podcast feed URL
                fieldName = 'podcastfeedurl'
            elif fieldID == '0x132': # App title
                pass
            elif fieldID == '0x190': # Podcast author (multiple)
                fieldName = 'authorname'
            elif fieldID == '0x191': # Artist name without 'The'; sort artist
                fieldName = 'artistshortername'
            elif fieldID == '0x1f4':
                pass
            elif fieldID == '0x1f7':
                #fieldName = '' ## looks like an identifier of some sort
                #processingType = 3
                pass
            elif fieldID == '0x1f8': # A UUID. For?
                pass
            elif fieldID == '0x1f9': # A UUID. For?
                pass
            elif fieldID == '0x1fa': # An email address. For?
                pass
            elif fieldID == '0x1fc':  #  Library share name                 --n/a--
                fieldName = 'libraryName'
            elif fieldID == '0x202': # A large one-off PLIST-type structure containing lots of keys etc. Present at the start of the file
                pass
            elif fieldID == '0x2bc': # Another plist
                pass
            elif fieldID == '0x2be': # 
                pass
            elif fieldID == '0x2bf': # 
                pass
            else:
                raise Warning ("Field not known: " + fieldID)
            if fieldName <> "":
                if processingType == 1:
                    unpkStr = endianPrefix + "2I I I I %ds" % (len(recordData)-20)
                    BlockData = unpack(unpkStr, recordData)
                    # self.data.['index'] = BlockData[0]  # not used in our context and, in this implementation, it
                                                            # gets overriden in super-structure's .attr[] by the next hohm entry
                    strEncoding = BlockData[1]
                    self.data['strLen'] = BlockData[2]
                    # Normally the next 2 entries are 0x00 - we're not checking it though
                    strData = BlockData[5]
                elif processingType == 2:
                    unpkStr = endianPrefix + "I %ds" % (len(recordData)-4)
                    BlockData = unpack(unpkStr, recordData)
                    strEncoding = 0 # seems we're only dealing with URLs
                    strData = BlockData[1]
                else: # libraryPersistentID
                    unpkStr = endianPrefix + "I Q"
                    BlockData = unpack(unpkStr, recordData)
                    strEncoding = BlockData[0]
                    strData = "{0:X}".format(BlockData[2])
                #fieldValue = str_decode(self.readPaddedContent(recordData, zeroPadding), strEncoding)
                fieldValue = str_decode(strData, strEncoding)
                self.attr[fieldName] = fieldValue
                    #debug_dump("Decoded: %s" % fieldValue)
        except (UserCancel, SystemExit):
            raise
        except Warning, e:
            print to_str(e)
            pass
        except:
            print traceback.print_exc()
            raise

        #####debug_dump("Returning from hohmBlock.__setAttributeFromFieldID()",-1)

    # For the record, here is my analysis of hohm type 1
    #      Byte   Length  Comment
    #    -----------------------
    #        0       4     'hohm'
    #        4       4     L = header length, usually 0x18
    #        8       4     R = total record length
    #       12       4     block type, i.e. 0x01
    #       16       2     seems to be 0x03 ?
    #       20      L-20   ? padding with 0x00 ?
    # ------ HEADER ENDS / CONTENT STARTS -----
    #        L       4     ? padding with 0x00 ?
    #       L+4      2     ??
    #       L+6      2     encoding ? (0x02)
    #       L+8      2     padding with 0x00 ?
    #       L+10     1     string size (max 27)
    #       L+11     27    volume name (e.g. hard drive name), padded with trailing 0x00 for length = 27
    #       L+38     2     ?? (iTunes 3)
    #       L+40     2     ??
    #       L+42     4     ??
    #       L+46     4     ??
    #       L+50     1     string size (max 64)
    #       L+51     64    file name, padded with trailing 0x00 for length = 64
    #       L+115    4     MacOS file type ?? (seems no longer the case)
    #       L+159    4     MacOS file creator ?? (seems no longer the case)
    #       L+163    7     ?? (full of 0x00)
    #       L+170    2     ? File folder count
    #       L+172    2     ? Library folder count
    #       L+174   13     ??
    #       L+175    1     n1 = string size
    #       L+176    n     possibly truncated album directory name (Windows-style ? possible as max length seems 31 char)
    #       L+176+n1 61     ??
    #       L+237+n1 3     0x00 02 00
    #       L+239+n1 1     n2 = string size
    #       L+241+n1 n2    Mac-style full file path
    #   m = n1+n2
    #       L+241+m  4     0x00 00 0E 00
    #       L+245+m  1     n3 = string size
    #       L+246+m  n3    utf-16 file name
    #   m2 = m + n3
    #       L+242+m2 4     0x00 0F 00
    #       L+246+m2 1     n4 = string size
    #       L+247+m2 n4    utf-16 volume name
    #   m3 = m2 + n4
    #       L+247+m3 3     0x00 12 00
    #       L+250+m3 1     n5 = string size
    #       L+251+m3 n5    Unix-style file path (hurrah !)
    #   m4 = m3 + n5 3     0x00 00 13 00
    #  etc. 
        

class hiimBlock(Block):
    # Preceded by hilm list super-block whose bytes 8-11 seem to contain the number of hiim items
    # item that describes author of album, video, etc., including any sorting author (e.g. "Beatles" for "The Beatles")
    # Header for record detail
    #     Byte   Length  Comment
    #    -----------------------
    #        0       4     'hohm'
    #        4       4     L = header length, usually 0x50
    #        8       4     R = total record length
    #       12       4     N = number of hohm sub-blocks
    #       16       4     item identifier
    #       20       8     persistent ID ?
    #       28       1      ?
    #       29       1      ? usually 0x02
    #       30      L-30   padding with 0x00 ?
    
    
    def loadFromString(self):
        global endianPrefix
        ####debug_dump("Entering hiimBlock.loadFromString()",1)
        try:
            unpkStr = endianPrefix + "I I Q 2B"
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.data['itemCount'] = BlockHeader[0]
            self.attr['entryID'] = BlockHeader[1]
            self.attr['persistentID'] = "{0:X}".format(BlockHeader[2])
            self.ITLFile.seek(self.headerLength - calcsize(unpkStr) - 12)
            i = 1
            while i <= self.data['itemCount']:
                subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                if subBlock.blockType == 'hohm':
                    self.attr.update(subBlock.attr)
                else:
                    raise Exception ("hohm block of index %d expected inside hiim" %i)
                i += 1
            ####debug_dump("hiim block #sub-items: %#x / Item index: %#x / Persistent ID: %#x / Content: %s" % (self.data['itemCount'], self.attr['entryID'], self.attr['persistentID'], self.attr.viewitems()))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        ####debug_dump("Returning from hiimBlock.loadFromString()",-1)



class haimBlock(Block):
    # Preceded by halm list super-block whose bytes 8-11 contain the number of haim items
    # "Aggregate/Album/App Item" (i.e. item to describe album/app, etc.)
    # Super-header for grouping records (albums + ?)
    #     Byte   Length  Comment
    #    -----------------------
    #        0       4     'haim'
    #        4       4     L = header length, usually 0x18
    #        8       4     R = total record length
    #       12       4     N = number of hohm sub-blocks
    #       16       4     entry identifier
    #       20       8     persistentID
    #       28     R-29    block specific data
    #      R-2       1     rating (100 scale)
    
    def loadFromString(self):
        global endianPrefix
        #####debug_dump("Entering haimBlock.loadFromString()",1)
        try:
            unpkStr = endianPrefix + "2I Q %dp B" % (self.headerLength-29)
            #####debug_dump("unpkstr: %s    size: %d" % (unpkStr,calcsize(unpkStr)))
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.data['subBlockCount'] = BlockHeader[0]
            self.attr['entryID'] = BlockHeader[1]
            self.attr['persistentID'] = "{0:X}".format(BlockHeader[2])
            self.data['headerData'] = BlockHeader[3]
            self.attr['albumRating'] = BlockHeader[4]/20
            #####debug_dump("haim block sub type: %#x / Album ID: %d / Persistent ID: %8x" % (self.data['hohmType'], self.attr['entryID'], self.attr['persistentID']))
            totalSize = self.headerLength
            while totalSize < self.contentLength:
                subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                self.attr.update(subBlock.attr)
                totalSize += subBlock.size
            #self.currentOffset += subPos
            #####debug_dump("haim data in sub-blocks: %s " % self.attr.viewitems())
        #self.STDB.addAlbum(STDBattr)
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #####debug_dump("Returning from haimBlock.loadFromString()",-1)


class hpimBlock(Block):
    # Header for playlists
    #    Byte   Length  Comment
    #    -----------------------
    #      0       4      hpim
    #      4       4      N = length of data
    #      8       4      R = total content length?
    #     12       4      index#?
    #     16       4      number of items (hptm) in playlist
    
    def loadFromString(self):
        global endianPrefix
        #####debug_dump("Entering hpimBlock.loadFromString()",1)
        try:
            unpkStr = endianPrefix + "2I"
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.data['unknown1'] = BlockHeader[0]
            self.data['entryID'] = BlockHeader[1]
            #####debug_dump("hpim block item count: %d / Marker: %d" % (self.data['itemCount'], self.data['unknown1']))
            self.ITLFile.seek(self.headerLength - calcsize(unpkStr) -12)
            i = 0
            totalSize = self.headerLength
            while totalSize < self.contentLength:
                subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                self.attr.update(subBlock.attr)
                i += 1
                totalSize += subBlock.size
            #####debug_dump("hpim data in sub-blocks: %s / # items: %d" % (self.attr.viewitems(),i))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #####debug_dump("Returning from hpimBlock.loadFromString()",-1)



class hptmBlock(Block):
    # Header for playlist items
    #    Byte   Length  Comment
    #    -----------------------
    #        0       4      hpim
    #        4       4      N = header length
    #        8       4      L = total content length (here L == N)
    #       12       4      ?
    #       16       4      ?
    #       20       4      ?
    #       24       4      song key (from htim)
    #       28     N - 28   ?
    
    def loadFromString(self):
        global endianPrefix
        #####debug_dump("Entering hptmBlock.loadFromString()",1)
        try:
            unpkStr = endianPrefix + "4I %ds" % (self.headerLength-28)
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            self.data['unknown1'] = BlockHeader[0]
            self.data['unknown2'] = BlockHeader[1]
            self.data['unknown3'] = BlockHeader[2]
            self.data['trackID'] = BlockHeader[3]
            self.data['headerData'] = BlockHeader[4]
            #####debug_dump("hptm block song key: %d / Markers: %d %d %d" % (self.data['songKey'], self.data['unknown1'], self.data['unknown2'], self.data['unknown3']))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #####debug_dump("Returning from hptmBlock.loadFromString()",-1)




class htimBlock(Block):
    #  Block for all track details
    #    Byte   Length  Comment
    #    -----------------------
    #      0       4     'htim'
    #      4       4     L = header length (usually 156, or 0x9C)
    #      8       4     R = total record length, including sub-blocks
    #     12       4     N = number of hohm sub-blocks
    #     16       4     track identifier
    #     20       4     block type => (1, ?)
    #     24       4     ?
    #     28       4     Mac OS file type (e.g. MPG3)
    #     32       4     modification date + time, base 1/1/1904 (at least on Mac)
    #     36       4     file size, in bytes
    #     40       4     playtime, millisecs
    #     44       4     track number
    #     48       4     total number of tracks
    #     52       2     ?
    #     54       2     year
    #     56       2     ? 0x00
    #     58       2     bit rate
    #     60       2     sample rate (seems no longer valid)
    #     62       2     ?
    #     64       4     volume adjustment (signed)
    #     68       4     start time, milliseconds
    #     72       4     end time, milliseconds
    #     76       4     playcount
    #     80       2     ? offset 52h, 0x0100 for song, 0x0500 for pdf
    #     82       1     ??
    #     83       1     compilation (1 = yes, 0 = no)
    #     84       4     ?
    #     88       4     ?
    #     92       2     category: Music (0x05), Book/Audio book (0x04), App (0x03) etc..???
    #     94       2     ??
    #     96       4     playcount again...
    #    100       4     last play date
    #    104       2     disk number
    #    106       2     total disks
    #    108       1     rating ( 0 to 100 )
    #    109      11     ?
    #    120       4     add date
    #    124       4     ? 
    #    128       8     persistentID
    #    136       4     ?
    #    140       4     Mac OS file type again?
    #    144      76     ?
    # ---- end block read in step 1
    #    220       4     album id (haim)
    # ---- end block read in step 2
    #    480       4     corresponding hiim id (album (author, sorting author) tuple)
    # ---- end of reading (then we skip to end of header)
    
    def loadFromString(self):
        global endianPrefix
        ##debug_dump("=== Entering htimBlock.loadFromString()",1)
        try:
            fieldMapping = OrderedDict([('subblockCount','I'), ('entryID','I'), ('htimType','I'), ('unknown1','I'), ('iosfiletype','4s'), ('modifiedDate','I'), ('fileSize','I'), ('playTime','I'), ('trackNumber','I'), ('trackCount','I'), ('unknown2','H'), ('year','H'), ('unknown3','H'), ('bitRate','H'), ('sampleRate','H'), ('unknown4','H'), ('volAdjustment','i'), ('startTime','I'), ('endTime','I'), ('playCount','I'), ('unknown5','H'), ('unknown5b','B'), ('compilation','B'), ('unknown6','I'), ('unknown7','I'), ('category','H'), ('unknown9','H'), ('playCount2','I'), ('lastPlayDate','I'), ('diskNumber','H'), ('diskCount','H'), ('rating','B'), ('unknown10','11s'), ('addDate','I'), ('unknown11','I'), ('persistentID','Q'), ('unknown12', 'I'),  ('iosfiletype2', '4s'), ('unknown13', '76s'), ('albumID', 'I')])
            unpkStr = endianPrefix
            for key in fieldMapping.iterkeys():
                unpkStr += fieldMapping[key] + " "
            ######debug_dump("unpkStr: %s / size: %d" % (unpkStr, calcsize(endianPrefix + unpkStr)))
            BlockHeader = unpack(unpkStr, self.ITLFile.read(calcsize(unpkStr)))
            i = 0
            for key in fieldMapping.iterkeys():
                if key.find('unknown') == -1:
                    if key == 'persistentID':
                        self.attr[key] = "{0:X}".format(BlockHeader[i])
                    elif key == 'rating':
                        self.attr[key] = BlockHeader[i]/20
                    else:
                        self.attr[key] = BlockHeader[i]
                i += 1
            i = 0
            curOffset = 12 + calcsize(unpkStr)
            ##debug_dump("********************* new offset %d then %d. Check %d" % (480-curOffset, self.headerLength - 484, self.headerLength))
            self.ITLFile.seek(480-curOffset)
            unpkStr = endianPrefix + "I"
            BlockHeader = unpack(unpkStr,self.ITLFile.read(calcsize(unpkStr)))
            self.attr['artistID'] = BlockHeader[0]
            self.ITLFile.seek(self.headerLength - 484)
            totalSize = self.headerLength
            while totalSize < self.contentLength:
                subBlock = Block(self.ITLFile, self.attributeProcessingCallback)
                self.attr.update(subBlock.attr)
                i += 1
                totalSize += subBlock.size
            if i <> self.attr['subblockCount']:
                raise Exception ("Incorrect number of sub-blocks for htim block")
            ##debug_dump("*** htim data in sub-blocks: %s " % self.attr.viewitems())
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            print to_str(e)
            raise e
        except:
            print traceback.print_exc()
            raise
        #finally:
            ##debug_dump("=== Returning from htimBlock.loadFromString()",-1)

