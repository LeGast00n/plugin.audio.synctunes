# Function: strip ITC files from (useless) headers to make them retrieve their JPG or PNG format
#
# Author: Louis-Arnaud Iscla
#
# (c) 2012, LAI <info at iscla net>
#
# References:
# http://www.falsecognate.org/2007/01/deciphering_the_itunes_itc_fil/

import struct
import binascii
import os

#
#import PIL.Image as Image
#import PIL.ImageFile as ImageFile
#import PIL.ContainerIO as ContainerIO
#
def _accept(prefix):
    return prefix[4:7] == "itc"

#
#class ItcImageFile(ImageFile.ImageFile):
#    # THIS CLASS IS SUPPOSED TO BE AN ADD-ON TO PYTHON IMAGING LIBRARY
#    # NOT TESTED
#    
#    format = "ITC"
#    format_description = "iTunes Cache Picture"
#    
#    def _open(self):
#        # check header
#        header = self.fp.read(7)
#
#        if header[4:7] != "itc":
#            raise SyntaxError, "not an ITC file"
# 
#        length1 = struct.unpack('B',header[3])[0]
#        
#        # skip the rest of file signature and the 256-byte buffer zone
#        # and skip data header (length of which is stored in 4 bytes starting on byte 9)
#        self.fp.seek(length1-7 + 256 + 4 + 4,1)
# 
#        length2=0
#        for i in range(4):
#            theByte = self.fp.read(1)
#            bin = struct.unpack('B',theByte)[0]
#            length2 = length2 * 256 + bin
#
#        # position the file pointer at the beginning of the actual image data
#        self.fp.seek(length2-12, 1)
#        imagestart = self.fp.tell()
#        self.fp.seek(0,2)
#        imagelength = self.fp.tell() - imagestart + 1
#        self.fp.seek(imagestart, 0)
#
#        self.fp = ContainerIO.ContainerIO(self.fp, imagestart, imagelength)
#        
#        self.newImage = Image.open(self.fp)
#        self.mode = self.newImage.mode
#        self.size = self.newImage.size
#        self.info = self.newImage.info
#        self.text = self.newImage.text # experimental
#        self.tile = self.newImage.tile
#        
#        if self.newImage.palette:
#            self.palette = self.newImage.palette
#    
#    
#    def verify(self):
#        return self.newImage.verify
#    
#    def load_prepare(self):
#        return self.newImage.load_prepare()
#    
#    def load_read(self, bytes):
#        return self.newImage.load_read(bytes)
#    
#    def load_end(self):
#        return self.newImage.load_end()
#
#    def verify(self):
#        return self.newImage.load()
#
#
#Image.register_open("ITC", ItcImageFile, _accept)
#
#Image.register_extension("ITC", ".itc")
#


JPGheader = binascii.a2b_hex("FFD8")
PNGheader = binascii.a2b_hex("89504e470d0a1a0a")


def renameIfPossible (originalName, newName):
    if os.path.isfile(newName):
        # file conflict, delete the tmp file
        os.unlink (originalName)
    else:
        os.rename (originalName, newName)
        return newName
    return

def itcToPictureFile (itcFileName, destPath=""):
    try:
        if not os.path.isdir(destPath):
            raise Exception("Directory " + destPath + " does not exist.")
        if not os.path.isfile(itcFileName):
            raise Warning("File " + itcFileName + " does not exist.")
        
                #thePic = Image.open(itcFileName)
                #print thePic.format
        
        shortName = os.path.basename(itcFileName)[:-3]
        
        itcFile = open(itcFileName, "rb")

        # skip file signature (length of which is stored in byte 4)
        itcFile.seek(3)
        theByte = itcFile.read(1)
        length = struct.unpack('B',theByte)[0]

        # skip the rest of file signature and the 256-byte buffer zone
        # and skip data header (length of which is stored in 4 bytes starting on byte 9)
        itcFile.seek(length-4 + 256 + 4 + 4,1)
        
        length=0
        for i in range(4):
            theByte = itcFile.read(1)
            bin = struct.unpack('B',theByte)[0]
            length = length * 256 + bin
                
        itcFile.seek(length-12,1)

        # now copy the rest of the file
        convertedFileName = os.path.join(destPath, shortName)
        
        header=itcFile.read(8)
        k = -1
        try:
            k = header.index(JPGheader)
            if k==0: extension = "jpg"
        except:
            try:
                k = header.index(PNGheader)
                if k==0: extension = "png"
            except:
                print "Image header not recognized for %s" %itcFileName
                return
            pass
        itcFile.seek(-8,1)

        with open(convertedFileName + "tmp", "wb") as convertedFile:
            convertedFile.write(itcFile.read())            
            convertedFile.close()
            itcFile.close()
            # rename the file according to its type (if no conflict)
            return renameIfPossible(convertedFileName + "tmp", convertedFileName + extension)
        itcFile.close()
        return
    except Exception, e:
        print repr(e)
        raise e


#fileName = itcToPictureFile("/Users/arnaud/Music/iTunes/Album Artwork/Cache/B40BBFF9272B486B/08/14/15/B40BBFF9272B486B-EFA794850DE18FE8.itc","/Users/arnaud/Desktop")

#im = Image.open("/Users/arnaud/Music/iTunes/Album Artwork/Cache/B40BBFF9272B486B/08/14/15/B40BBFF9272B486B-EFA794850DE18FE8.itc")





