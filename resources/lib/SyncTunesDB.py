"""
    Class for managing SyncTunes' database
"""

__author__  = "LAI, <info at iscla net>"
__credits__ = "AlfredJKwack, Anoop Menon, jingai, the mutagen team"
__url__     = ""


import traceback
import xml.parsers.expat
import sys
#sys.path.append("./sqlite3")
#sys.path.append("./PIL")
from sqlite3 import dbapi2 as sqlite
import os
import os.path
import locale
from mutagen import File as mutagenFile
from json import dumps
from itertools import ifilterfalse
from ItcImagePlugin import itcToPictureFile
from utilFunctions import *


global separator
separator = "/"


class SyncTunesDB(object):
    
    DB_VERSION = '1.0' # would change with evolutions of this module
    
    def __DBconnect(self, theFilePath = ''):
        try:
            if theFilePath == '': theFilePath = self.dbFilePath
            try:
                self.dbconn.close()
            except:
                pass
            self.dbconn = sqlite.connect(theFilePath)
            self.dbconn.execute("PRAGMA synchronous = OFF")
            self.dbconn.execute("PRAGMA default_synchronous = OFF")
            self.dbconn.execute("PRAGMA journal_mode = OFF")
            self.dbconn.execute("PRAGMA temp_store = MEMORY")
            self.dbconn.execute("PRAGMA encoding = \"UTF-16BE\"")
            self.dbconn.row_factory = sqlite.Row
        except Exception, e:
            debug_dump(to_str(e))
            raise e

    def __resetCaches(self):
        self.cache = {'lastartist': {}, 'lastalbumartist': {}, 'lastcomposer': {}, 'lastalbum': {}}
        self.pathCache = {}
#        if reset:
#            self.genres = {}
#            self.inv_genres = {}
#        else:
        theGenres = self._select('genres',{})
        self.genres = dict((theGenres[k]['id'], theGenres[k]['name']) for k in range (0,len(theGenres)))
        self.inv_genres = dict((v, k) for k,v in self.genres.items())


    def __init__(self, dataPath, libFile, addon = None, reset = False):
#        debug_dump("Entering SyncTunesDB.__init__()", 1)
        self.readyForUpdate = False
        self.dataPath = os.path.dirname(dataPath)
        self.dbFilePath = dataPath
        self.artworkPath = os.path.join(self.dataPath, 'Artwork')
        self.libFilePath = libFile
        self.config = {}
        self.addon = addon
        self.readyForUpdate = False
        try:
            self.__DBconnect()        
            self.__prepareDB(reset)
            self.__resetCaches()
        except:
            debug_dump(traceback.format_exc())
            raise
#        debug_dump("Returning from SyncTunesDB.__init__()", -1)
        return

    def __prepareDB (self, reset = False):
        ###debug_dump("Entering SyncTunesDB.__prepareDB()", 1)
        # check the version of the data model of the current database
        # if it does not correspond to the version of this module, then
        # reset the database and re-initialize it
        try:
            tableExists = False
            cur = self.dbconn.cursor()
            if not reset:
                cur.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='config'""")
                tableExists = cur.fetchone()
            if (tableExists):
                currentDBVersion = self.getConfigItem("DatabaseModelVersion")
            if (reset or not tableExists or currentDBVersion <> self.getConfigItem("DatabaseModelVersion")):
                if reset:
                    debug_dump("Resetting database...")
                else:
                    debug_dump("Resetting database owed to model version change")
                self.ResetDB()
                self.setConfigItem("DatabaseModelVersion", self.DB_VERSION)
                self.setConfigItem("Library File Path", self.libFilePath)
                self.setConfigItem("Status", "Incomplete")
        except:
            debug_dump(traceback.format_exc())
            pass
        ###debug_dump("Returning from SyncTunesDB.__prepareDB()", -1)
        return

    def prepareForUpdate(self, newDBFile = True):
        # marking all entries as stale (they will be removed after the update if they are still marked as such)
        if newDBFile:
            self.newDbFilePath = os.path.splitext(self.dbFilePath)[0] + "_NEW.db"
            self.__DBconnect(self.newDbFilePath)
            self.__prepareDB()
            self.__resetCaches()        
        else:
            cur = self.dbconn.cursor()
            for table in ['tracks','albums','artists','playlists','playlisttracks']:
                cur.execute("UPDATE " + table + " SET uptodate = 0" )
        self.__commit()
        if newDBFile:
            # disconnect from the DB, rename the file of the new DB
            self.dbconn.close()
            try:
                os.remove(os.path.splitext(self.dbFilePath)[0] + "_OLD.db")
            except:
                pass
            try:
                # rename the current DB with an "_OLD" suffix, it should enable any running query to complete fine
                os.rename(self.dbFilePath, os.path.splitext(self.dbFilePath)[0] + "_OLD.db")
                # rename the new DB to make it the principal DB
                os.rename(self.newDbFilePath, self.dbFilePath)
                del self.newDbFilePath
            except:
                debug_dump(traceback.format_exc())
                raise
            self.__DBconnect()
            
        self.readyForUpdate = True

            
    def getStatus():
        status = self.getConfigItem("Status")
        if status == None: return "Incomplete"


    def localizeString(self, theString):
        try:
            theString = self.addon.getLocalizedString(theString)
            return theString
        except:
            return "Unknown"


    def InitDB(self):
        #        try:
        #    self.dbconn.execute("PRAGMA synchronous = OFF")
        #    self.dbconn.execute("PRAGMA default_synchronous = OFF")
        #    self.dbconn.execute("PRAGMA journal_mode = OFF")
        #    self.dbconn.execute("PRAGMA temp_store = MEMORY")
        #    self.dbconn.execute("PRAGMA encoding = \"UTF-16BE\"")
        #except Exception, e:
        #    debug_dump(to_str(e))
        #    pass
        try:
            # config table
            self.dbconn.execute("""
            CREATE TABLE config (
               key varchar primary key,
               value varchar
            )""")
        except:
            pass
        
        try:
            # tracks table
            self.dbconn.execute("""
            CREATE TABLE tracks (
               id integer primary key autoincrement,
               title varchar,
               persistent varchar,
               tracknumber integer DEFAULT 0,
               disknumber integer DEFAULT 0,
               year integer,
               duration integer,
               albumid integer DEFAULT 0,
               genreid integer DEFAULT 0,
               artistid integer DEFAULT 0,
               composerid integer DEFAULT 0,
               playcount integer DEFAULT 0,
               rating integer DEFAULT 0,
               filename varchar,
               uptodate integer DEFAULT 1,
               artworkpath varchar,
               UNIQUE (id, persistent)
            )""")
        except:
            pass

        try:
            # genres table
            self.dbconn.execute("""
            CREATE TABLE genres (
               id integer primary key autoincrement,
                name varchar,
                uptodate integer DEFAULT 1
            )""")
        except:
            pass

        try:
            # artists table
            self.dbconn.execute("""
            CREATE TABLE artists (
               id integer primary key autoincrement,
               name varchar,
               istrackartist integer DEFAULT 0,
               isalbumartist integer DEFAULT 0,
               iscomposer integer DEFAULT 0,
               uptodate integer DEFAULT 1
                )""") #            sortname varchar,  // , UNIQUE (id, name)
        except:
            pass

        try:
            # albums table
            self.dbconn.execute("""
            CREATE TABLE albums (
               id integer primary key,
               name varchar,
               persistent varchar,
               artistid integer DEFAULT 0,
               rating integer DEFAULT 0,
               compilation integer DEFAULT 0,
               artworkpath varchar,
               uptodate integer DEFAULT 1
            )""")
        except:
            pass

        try:
            # playlists
            self.dbconn.execute("""
            CREATE TABLE playlists (
               id integer primary key autoincrement,
               persistent varchar,
               name varchar,
               master integer,
               visible integer,
               allitems integer,
               uptodate integer DEFAULT 1
            )""")
        except:
            pass

        try:
            # playlist tracks
            self.dbconn.execute("""
            CREATE TABLE playlisttracks (
               playlistid integer,
               trackid integer,
               playorder integer,
               uptodate integer DEFAULT 1
            )""")
        except Exception, e:
            pass


    def ResetDB(self):
        for table in ['tracks','albums','artists','playlists',
                      'playlisttracks','genres']:
            try:
                self.dbconn.execute("DROP TABLE IF EXISTS %s" % table)
            except Exception, e:
                debug_dump(to_str(e))
                pass
        try:
            self.InitDB()
        except Exception, e:
            debug_dump(to_str(e))
            raise e


    def commit(self):
        # performs some housework...
        queryList = []
        queryList.append("DELETE FROM artists WHERE uptodate = 0")
        queryList.append("DELETE FROM albums WHERE uptodate = 0")
        queryList.append("DELETE FROM tracks WHERE uptodate = 0")
        queryList.append("DELETE FROM genres WHERE uptodate = 0")
        queryList.append("DELETE FROM albums WHERE NOT EXISTS (SELECT T.id FROM tracks T WHERE T.albumid = albums.id)") # albums with no track
        queryList.append("DELETE FROM genres WHERE NOT EXISTS (SELECT T.id FROM tracks T WHERE T.genreid = genres.id)") # genres not referred to
        queryList.append("UPDATE artists SET istrackartist=EXISTS (SELECT T.id FROM tracks T WHERE T.artistid = artists.id)")
        queryList.append("UPDATE artists SET isalbumartist=EXISTS (SELECT L.id FROM albums L WHERE L.artistid = artists.id)")
        queryList.append("UPDATE artists SET iscomposer=EXISTS (SELECT T.id FROM tracks T WHERE T.composerid = artists.id)")
        for queryStr in queryList:
            try:
                self.dbconn.execute(queryStr)
            except:
                debug_dump(traceback.format_exc())
                pass
        self.__commit()
        self.setConfigItem("Status", "Complete")
        self.readyForUpdate = False

    
    def __commit(self):
        try:
            self.dbconn.commit()
        except Exception, e:
            debug_dump("Commit Error: " + to_str(e))
            pass        
            
    def getConfigItem(self, key):
        try:
            if self.config != None and self.config[key] != None:
                return self.config[key]
        except:
            pass
        try:
            cur = self.dbconn.cursor()
            cur.execute("""SELECT value
                           FROM config 
                           WHERE key like ? LIMIT 1""",
                        (key,))
            row = cur.fetchone()
            if (row):
                self.config[key] = row[0]
                return row[0]
            return None
        except:
            return None

    def setConfigItem(self, key, value):
        if (self.getConfigItem(key)==None):
            cur = self.dbconn.cursor()
            cur.execute("""INSERT INTO config(key,value)
                           VALUES(?,?)""",
                        (key, value))
            self.__commit()                        
        else:
            cur = self.dbconn.cursor()
            cur.execute("""UPDATE config
                           SET value=?
                           WHERE key=?""",
                        (value, key))
            self.__commit()
        self.config[key] = value

    def UpdateLastImport(self):
        self.setConfigItem('lastimport', 'dummy')
        self.dbconn.execute("""UPDATE config
                               SET value=datetime('now')
                               WHERE key=?""",
                            ('lastimport',))
        self.__commit()

    #def updateRecordInTable(self, table, attributeSet, fields=None):
    

###
# LOW LEVEL METHODS: select, update, insert
#
# no cache management
###

    

    @staticmethod
    def typeop(typeOrVar):
        if isinstance(typeOrVar, basestring):
            return "like"
        if isinstance(typeOrVar,type) and typeOrVar == str:
            return "like"
        else:
            return "="


    def _select(self, table, criteria, singleRow=0):
        #debug_dump("Entering SyncTunesDB._select()", 1)
        try:
            # query db for column with specified name
            #it = dict([(x,fields[x]) for x in filter(fields.has_key,attributeSet.keys())]).iteritems()

            # I'm proud of the below line :-) (good luck)
            whereStr = " AND ".join(map("{0[0]} {0[1]} :{0[0]}".format, [(f,self.typeop(v)) for f,v in criteria.items()]))
            if whereStr <> "": whereStr = "WHERE " + whereStr
            
            queryStr = '''SELECT * FROM %s %s''' % (table, whereStr)
            cur = self.dbconn.cursor()
            cur.execute(queryStr, criteria)
            desc = cur.description
            if singleRow: result= [cur.fetchone()]
            else: result= cur.fetchall()
            if result == [None]: result = []
            return result
        except:
            #print "SELECT error with %s" % queryStr
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from SyncTunesDB._select(), len: %d" % len(result), -1)
    
                   
    def _update(self, table, attributeSet, fields=None):
        #debug_dump("Entering SyncTunesDB._update()",1)
        queryStr = ""
        try:
            if not self.readyForUpdate: raise Exception("Database has not been prepared for update, use prepareForUpdate() first")
            if fields == None: fields = {'name':str}
            if isinstance(attributeSet, sqlite.Row):
                attributeSet = dict([(x, attributeSet[x]) for x in attributeSet.keys()])
            attributeSet.update({'uptodate': 1})
            #build string after VALUES
            #only the fields that are not part of the criteria (i.e. fields) will be assigned
            newVal = dict([(x,attributeSet[x]) for x in ifilterfalse(fields.has_key, attributeSet.iterkeys())])
            valStr = ", ".join("%s = :%s" %(x,x) for x in newVal.iterkeys())

            it = dict([(x,fields[x]) for x in filter(fields.has_key,attributeSet.keys())])
        
            # I'm proud of the below line :-) (good luck)
            whereStr = " AND ".join(map("{0[0]} {0[1]} :{0[0]}".format, [(f,self.typeop(v)) for f,v in it.items()]))
            if whereStr <> "": whereStr = "WHERE " + whereStr
            
            queryStr = '''UPDATE %s SET %s %s;''' % (table, valStr, whereStr)
            cur = self.dbconn.cursor()
            cur.execute(queryStr, attributeSet)
            self.__commit()
            return cur.rowcount
        except:
            debug_dump("UPDATE error with %s  / %s" % (queryStr, attributeSet))
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from SyncTunesDB._update(): %d" % cur.rowcount, -1)


    def _insert(self, table, attributeSet):
        #debug_dump("Entering SyncTunesDB._insert(%s,%s)"%(table, attributeSet), 1)
        queryStr = ""
        cur = self.dbconn.cursor()
        try:
            if not self.readyForUpdate: raise Exception("Database has not been prepared for update, use prepareForUpdate() first")
            fieldStr = ", ".join("%s" %x for x in attributeSet.iterkeys())
            valueStr = ", ".join(":%s" %x for x in attributeSet.iterkeys())
            
            queryStr = '''INSERT INTO %s (%s) VALUES (%s)''' % (table, fieldStr, valueStr)
            cur.execute(queryStr, attributeSet)
            self.__commit()
            if cur.lastrowid == 0:
                debug_dump("CAUTION: row inserted with resulting id = 0" %queryStr)
            return cur.lastrowid
                # except IntegrityError:
                #debug_dump ("Integrity Error with %s" % queryStr)
        except:
            debug_dump("INSERT error with %s" % queryStr)
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from SyncTunesDB._insert(): %d" % (cur.lastrowid if cur.lastrowid<> None else -1), -1)


    ###
    # Utility methods
    ###


    def _lookupRecordInTable(self, table, attributeSet, fields=None, mode=0, autoclean=True):
        # mode == 0: no autoadd, mode == 1: autoadd (if absent), mode == 2: add or update if present
        # returns a single record of type Dictionary
        # returns None if record not found and not created
        #debug_dump("Entering SyncTunesDB._lookupRecordInTable()", 1)
        try:
            if fields == None:
                fields = {'name':str}
            #Manage "unknown" here
            #if (autoclean and not attributeSet[column]):
            #    attributeSet[column] = self.localizeString(30302)
            theRow = self._select(table, dict([(x,attributeSet[x]) for x in filter(fields.has_key,attributeSet.keys())]), True)
            if len(theRow) > 0:
                if mode <= 1 and ('uptodate' in theRow[0].keys() and theRow[0]['uptodate'] == 0 or theRow[0]['uptodate'] == None):
                    # let's just get ready to mark the record as up to date
                    attrSet = {'id': theRow[0]['id']}
                    flds = {'id': int}
                elif mode == 2:
                    attrSet = attributeSet
                    flds = fields
                else:
                    return theRow[0]
                attrSet.update({'uptodate': 1})
                ret = self._update(table, attrSet, flds)
                if ret <= 0:
                    raise Exception ("Error: there should have been a row id %d to UPDATE but it could not be." %theRow[0]['id'])
                return theRow[0]
            elif mode > 0:
                id = self._insert(table, attributeSet)
                if id == None:
                    raise Exception ("Cannot INSERT to database %s." %repr(attributeSet))
                attributeSet.update({'id': id})
                return attributeSet
                    
            try:
                return theRow[0] # return id
            except:
                return None
        except Exception, e:
            debug_dump(to_str(e))
            pass
        except:
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from SyncTunesDB._lookupRecordInTable()", -1)


    def compareRecords(self, soughtRecord, cacheRecord, comparisonFields):
        ##debug_dump("Entering SyncTunesDB.compareRecords(%s, %s, %s)" % (soughtRecord, cacheRecord, comparisonFields), 1)
        try:
            # old-fashioned algo - keeping it here as the new-fashioned has once produced erratic errors
            #            match = True
            #            i = iter(comparisonFields)
            #            try:
            #                field = i.next()
            #                while match:
            #                    match = ((field in soughtRecord) == (field in cacheRecord))
            #                    match = match and (soughtRecord[field] == cacheRecord[field])
            #                    field = i.next()
            #            except StopIteration:
            #                    pass
            #            ###debug_dump("Returning %d from SyncTunesDB.compareRecords()" % match, -1)
            #             return match
            return cacheRecord <> {} and ([(x,soughtRecord[x]) for x in comparisonFields.iterkeys()] == [(x,cacheRecord[x]) for x in comparisonFields.iterkeys()])
        except KeyError:
            return False
        except:
            debug_dump(traceback.format_exc())
            raise
        #finally:
            ##debug_dump("Returning from SyncTunesDB.compareRecords()", -1)


    def getRecordFromAttributes(self, table, recordAttributes, keyFields = {}, mode = 0, recordType = None):
            # mode == 0: no autoadd, mode == 1: autoadd (if absent), mode == 2: add or update if present
            # return the id of the record corresponding to the criteria
            # can add if nonexistent
            # recordType can be 'artist', 'albumartist', 'composer' or 'album' (used for cache)
            # keyFields = {'name': str,'sortname': str}  # these must be strictly equal (if absent, assumed empty string) for concluding two records are identical
            try:
                #debug_dump("Entering SyncTunesDB.getRecordFromAttributes(%s, %s, %s, %s)" % (table, repr(recordAttributes), repr(keyFields), recordType if recordType <> None else "<None>"), 1)
                if keyFields == {}:
                    if table == 'artists':
                        keyFields = {'name': str} #, 'sortname': str}
                    elif table == 'albums':
                        keyFields = {'persistent': int, 'id': name}
                    elif table == 'tracks':
                        keyFields = {'persistent': int}
                    elif table == 'genres':
                        keyFields = {'name': str}
                    elif table == 'playlists':
                        keyFields = {'id': int}
                    else:
                        raise Exception ("getRecordFromAttributes() unaware on how to deal with %s." % table)
    
                # complement record with any missing field, set to empty string
                recordAttributes.update([(x, "" if keyFields[x] == str else 0) for x in ifilterfalse(recordAttributes.has_key, keyFields.iterkeys())])
                # compare with cache, only on the keys of the record
                cacheHit=False
                if not recordType == None:
                    theCache = self.cache['last' + recordType]
                    cacheHit= self.compareRecords(recordAttributes, self.cache['last' + recordType], keyFields)
                        
                    ##debug_dump("*** CACHE HIT = %d / %s // %s ***"%(cacheHit, repr(recordAttributes), repr(theCache)))
                if cacheHit:
                    return theCache
                else:
                    result = self._lookupRecordInTable(table, recordAttributes, keyFields, mode)
                    if recordType <> None and result <> None:
                        self.cache['last' + recordType] = result
                    if result == None: result = {'id': 0}
                    return result
            except Exception, e:
                debug_dump(to_str(e))
                raise e
            except:
                debug_dump(traceback.format_exc())
                raise
            #finally:
                #debug_dump("Returning SyncTunesDB.getRecordFromAttributes()", -1)

                    
    def checkMandatoryAttributes(self, attributes, mandatoryAttributes):
        try:
            [(x,mandatoryAttributes[x]) for x in filter(attributes.has_key, mandatoryAttributes.iterkeys())]
            return True
        except KeyError,e:
            return False

#####
# High-level creation methods
####
        
    def findOrCreateArtist(self, attributes, artistType = 'artist'):
        # artistType can be 'artist' (track artist), 'album artist', 'composer'
        mandatoryAttributes = ['name'] #, 'sortname']
        try: # don't want to block if there is no artist associated to the track
            #debug_dump("Entering SyncTunesDB.findOrCreateArtist(%s, %s)" % (repr(attributes), artistType), 1)
            if attributes == {} or ('name' not in attributes): # and 'sortname' not in attributes):
                attributes.update({'id': 0})
                return attributes
            if 'name' not in attributes: attributes.update({'name': ""})
            #if 'sortname' not in attributes: attributes.update({'sortname': ""})
                #if 'id' in attributes and artistType == 'albumartist':
                ## this happens for an albumartist when the album's entry has already led to creation
                ## of a (partially-filled) artist, that is we are in the track creation step and we're missing the sortname
                #fields = {'name': str, 'id': int}
                #   else:
            fields = {'name': str} #, 'sortname': str}
            missing = [ x  for x in ifilterfalse(attributes.has_key, mandatoryAttributes)]
            if len(missing) > 0:
                raise Exception ("Error: missing attributes %s, cannot upload album." %missing)
            result = self.getRecordFromAttributes('artists', attributes, fields, True, artistType)
#            if self.getArtists() == []:
#                raise BaseException ("DB corrupted on artist %s" % repr(attributes))
            return result
        except Exception, e:
            debug_dump(to_str(e))
            return
        except:
            debug_dump(traceback.format_exc())
            raise
#        finally:
#            debug_dump("Returning from SyncTunesDB.findOrCreateArtist()", -1)

        
                
    def findOrCreateAlbum(self, attributes, autoadd = True):
        debug_dump("Entering SyncTunesDB.findOrCreateAlbum(%s)" % repr(attributes), 1)
        if autoadd:
            criteria = {'persistent': int}
            mandatoryAttributes = ['name', 'persistent']
        else:
            criteria = {'name': str}
            mandatoryAttributes = ['name']
        # optional attributes: albums.albumartist
        try:
            attr = ""
            result = ""
            missing = [ x  for x in ifilterfalse(attributes.has_key, mandatoryAttributes)]
            if len(missing) > 0:
                attributes.update({'id': 0})
                return attributes
            result = self.getRecordFromAttributes('albums', attributes, criteria, autoadd, 'album')
            # we should consider updating artwork only when coming from the declaration of a track (at which time we have the necessary details)
            # this is tested by testing 'trackpersistent' entry
            updateArtwork = (attributes.has_key('trackpersistent') and result['id'] <> 0)
            try:
                if updateArtwork and result['artworkpath'] <> None and result['artworkpath'] <> '':
                    if self.translatePath(result['artworkpath']): # Here we test if the path to the file exists
                        updateArtwork = False
                    else:
                        debug_dump("Path nonexistent: %s" % result['artworkpath'])
            except:
                pass
            if updateArtwork:
                ### let's process the artwork here
                # convert from isqlrow to dict
                attr = dict([(x, result[x]) for x in result.keys()])
                #attr.update({'firsttrackid': attributes['firsttrackid']})
                #res = self._update('albums', attr, {'id': int}) ## update the firsttrackid field
                #if res <= 0:
                    #raise Exception ("Error: there should have been a row id %d to UPDATE but it could not be." %theRow[0]['id'])
                artworkPath = self.getArtworkPath(attr['persistent'])
                if artworkPath == None:
                    artworkPath = self.getArtworkPath(attributes['trackpersistent'])
                if artworkPath == None:
                    artworkPath = attributes.get('filename','')
                    artworkPath = self.getAudioFileArtwork(uriToPath(artworkPath))
                ### here we may need to convert ITC to image file and adapt path accordingly
                ### to be implemented if necessary
                if artworkPath <> None and artworkPath <> '':
                    attr.update({'artworkpath': artworkPath})
#                    debug_dump("Artwork update: %s" % repr(attr))
                self.cache['lastalbum'] = attr
                affected = self._update('albums', attr, {'id': int}) ## update the firsttrackid field
                if affected <= 0:
                    raise Exception ("Error: there should have been a row id %d to UPDATE but it could not be." % attr['id'])
                result = attr


#            if self.getAlbums() == []:
#                raise BaseException ("DB corrupted on album %s" % repr(attributes))
#            else:
#                print ("OK album")
            return result
        except Exception, e:
            debug_dump(to_str(e) + " " + repr(attr))
            debug_dump(traceback.format_exc())
            raise
        finally:
            debug_dump("Returning from SyncTunesDB.findOrCreateAlbum(). Returning %s" %result, -1)


    def updateOrCreateTrack(self, attributes):
        mandatoryAttributes = ['title', 'id', 'persistent']
        artistAttributes = ['artistid', ]
        # optional attributes: albums.albumartist
        try:
            #debug_dump("Entering SyncTunesDB.updateOrCreateTrack(%s)" % repr(attributes), 1)
            missing = [ x  for x in ifilterfalse(attributes.has_key, mandatoryAttributes)]
            if len(missing) > 0:
                raise Exception ("Error: missing attributes %s, cannot upload track." %missing)

            try:
                attributes['filename'] = uriToPath(attributes['filename'])
            except Exception:
                pass
            if attributes.get('artworkpath', '') in ('', None) and self.audioFileHasArtwork(attributes['filename']):
                attributes.update({'artworkpath': ''}) # we don't want to override the file's artwork with the artwork of the album
#            else:
#                if attributes.has_key('artworkpath'): debug_dump("File %s should be set with album's artworkpath %s" % (attributes['filename'], attributes['artworkpath']))
#                else: debug_dump("Likely no artwork for file %s" % attributes['filename'])
            result = self.getRecordFromAttributes('tracks', attributes, {'persistent': int}, 2)# , 'id': int
            return result
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))
            pass
        except:
            debug_dump(traceback.format_exc())
            raise
#        finally:
#            debug_dump("Returning from SyncTunesDB.updateOrCreateTrack()", -1)


    def findOrCreateGenre(self, attributes):
        try:
            #debug_dump("Entering SyncTunesDB.findOrCreateGenre(%s)" % repr(attributes), 1)
            result = {'id':0}
            theGenre = attributes.get('name')
            if theGenre == '' or theGenre == None: return result
        
            if theGenre in self.genres.values():
                #debug_dump("CACHE HIT %s %d" % (theGenre, self.inv_genres[theGenre]))
                attributes.update({'id':self.inv_genres[theGenre]})
                result = attributes
            else:
                result = self.getRecordFromAttributes('genres', attributes, {'name': str}, True)
                if result['id'] <> 0:
                    self.genres.update({result['id']:theGenre})
                    self.inv_genres.update({theGenre:result['id']})
            return result
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))
            return result
        except:
            debug_dump(traceback.format_exc())
            raise
        #finally:
            #debug_dump("Returning from SyncTunesDB.findOrCreateGenre(%s)" %repr(result), -1)
            


#############
#############



    def AddPlaylistNew(self, playlist):
        if not playlist['Playlist ID']:
            return
        try:
            self.dbconn.execute("""
                INSERT INTO playlists
                (id, persistent, name, master, visible, allitems)
                VALUES (?,?,?,?,?,?)""",
                                (playlist['Playlist ID'],
                                 playlist['Playlist Persistent ID'],
                                 playlist['Name'],
                                 playlist['Master'],
                                 playlist['Visible'],
                                 playlist['All Items']))
        except Exception, e:
            debug_dump(to_str(e))
        try:
            order = 1
            playlistid = playlist['Playlist ID']
            for track in playlist['tracklist']:
                self.AddTrackToPlaylist(playlistid, track, order)
                order += 1
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise

    def AddTrackToPlaylist(self, playlistid, trackid, order):
        if not playlistid or not trackid:
            return
        try:
            self.dbconn.execute("""
                INSERT INTO playlisttracks ( playlistid, trackid, playorder )
                VALUES (?,?,?)""", (playlistid, trackid, order))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))


#############
#############

    def _retrieveSelectResults(self, queryStr, queryParams=()):
        try:
            cur = self.dbconn.cursor()
            cur.execute(queryStr, queryParams)
            desc = cur.description
            return cur.fetchall()
            #the below is dealt with by row_factory
            #row = cur.fetchone()
            #result = []
            #while not row == None:
                #result.append(dict([(desc[x][0],row[x]) for x in range(0,len(desc))]))
            #row = cur.fetchone()
            #return result
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []

            
    def getTracks(self):
        try:
            queryStr = """SELECT T.id, T.title, T.playcount, T.rating, T.year,
            T.disknumber, T.tracknumber, T.filename, T.artworkpath FROM tracks T"""
            queryParams = ()
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []


    def getTracksByArtist(self, artistid, artistCategory='artist'):
        try:
            try:
                dbfield = ({'artist': 'T.artistid', 'composer': 'T.composerid'})[artistCategory] #, 'albumartist': 'L.artistid'
            except KeyError:
                dbfield = 'istrackartist'
            queryStr = """SELECT T.id, T.title, T.playcount, T.rating, T.year,
                T.disknumber, T.tracknumber, T.filename, T.artworkpath,
                L.name as album, A.name as artist, T.duration, G.name as genre
                FROM tracks T
                LEFT JOIN albums L ON T.albumid = L.id
                LEFT JOIN artists A ON T.artistid = A.id
                LEFT JOIN genres G ON T.genreid = G.id
                WHERE T.%s = ?
                ORDER BY T.title""" % dbfield # L.name, T.disknumber, T.tracknumber"""
            queryParams = (artistid,)
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []


    def getTracksInAlbum(self, albumid):
        try:
            queryStr = """SELECT T.id, T.title, T.playcount, T.rating, T.year,
                T.disknumber, T.tracknumber, T.filename, T.artworkpath,
                L.name as album, A.name as artist, T.duration, G.name as genre
                FROM tracks T
                LEFT JOIN albums L ON T.albumid = L.id
                LEFT JOIN artists A ON T.artistid = A.id
                LEFT JOIN genres G ON T.genreid = G.id
                WHERE T.albumid = ?
                ORDER BY L.name, T.disknumber, T.tracknumber"""
            queryParams = (albumid,)
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []



    def getTracksInGenre(self, genreid):
        try:
            queryStr = """SELECT T.id, T.title, T.playcount, T.rating, T.year,
                T.disknumber, T.tracknumber, T.filename, T.artworkpath,
                L.name as album, A.name as artist, T.duration, G.name as genre
                FROM tracks T
                LEFT JOIN albums L ON T.albumid = L.id
                LEFT JOIN artists A ON T.artistid = A.id
                LEFT JOIN genres G ON T.genreid = G.id
                WHERE T.genreid = ?
                ORDER BY L.name, T.disknumber, T.tracknumber"""
            queryParams = (genreid,)
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []


    def getTracksWithRating(self, rating):
        try:
            queryStr = """SELECT T.id, T.title, T.playcount, T.rating, T.year,
                T.disknumber, T.tracknumber, T.filename, T.artworkpath,
                L.name as album, A.name as artist, T.duration, G.name as genre
                FROM tracks T
                LEFT JOIN albums L ON T.albumid = L.id
                LEFT JOIN artists A ON T.artistid = A.id
                LEFT JOIN genres G ON T.genreid = G.id
                WHERE T.rating = ?
                ORDER BY L.name, T.disknumber, T.tracknumber"""
            queryParams = (rating,)
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []



    def getArtists(self, artistCategory=''):
        debug_dump("Entering getArtists(%s)" % repr(artistCategory),1)
        try:
            try:
                dbfield = ({'artist': 'istrackartist', 'albumartist': 'isalbumartist', 'composer': 'iscomposer'})[artistCategory]
                whereStr = "WHERE %s = 1" % dbfield
            except KeyError:
                dbfield = 'istrackartist'
                whereStr = ""
            queryStr = """SELECT id, name FROM artists %s""" %whereStr
            queryParams = ()
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        finally:
            debug_dump("Exiting getArtists()",-1)


    def getGenres(self):
        try:
            queryStr = """SELECT id,name FROM genres"""
            return self._retrieveSelectResults(queryStr, ())
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []


    def getAlbums(self):
        try:
            queryStr = """SELECT L.id, L.name, L.artistid, L.rating, A.name as artist, L.artworkpath FROM albums L LEFT JOIN artists A ON L.artistid = A.id """
            queryParams = ()
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []


    def getAlbumsByArtistId(self, id):
        try:
            queryStr = """SELECT L.id, L.name, L.artworkpath, L.rating, A.name as artist FROM albums L LEFT JOIN artists A ON L.artistid = A.id WHERE artistid=?"""
            queryParams = (id,)
            return self._retrieveSelectResults(queryStr, queryParams)
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            pass
        return []


### A faire

    def GetPlaylists(self):
        playlists = []
        try:
            cur = self.dbconn.cursor()
            cur.execute("SELECT id,name FROM playlists WHERE visible != 'false'")
            for tuple in cur:
                playlists.append(tuple)
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))
            pass
        return playlists

            
    def GetTracksInPlaylist(self, playlistid):
        tracks = []
        if not playlistid:
            return None
        try:
            cur = self.dbconn.cursor()
            cur.execute("""SELECT T.id, T.name, T.playcount, T.rating, T.year,
                                  T.bitrate, T.albumtracknumber, T.filename,
                                  L.name as album, A.name as artist, T.playtime,
                                  G.name as genre, W.relativePath
                           FROM playlisttracks P
                                LEFT JOIN tracks T ON P.trackid=T.id
                                LEFT JOIN albums L ON T.albumid = L.id
                                LEFT JOIN artists A ON T.artistid = A.id
                                LEFT JOIN genres G ON T.genreid = G.id
                           WHERE P.playlistid = ?
                           ORDER BY P.playorder """, (playlistid,))
            for tuple in cur:
                track = self._track_from_tuple(tuple)
                tracks.append(track)
            return tracks
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))
            pass
        return tracks

###
### MANAGEMENT OF THE PATH TRANSLATION
###


    def __addToPathCache(self, oriDir, transDir, branch):
        # adds to the path translation cache all the sub-directories as per the branch's depth
        #debug_dump("Entering __addToPathCache(%s,%s,%s)" % (oriDir, transDir, branch))
        global separator
        try:
            subBranches = branch.split(separator)
            subBranch = transDir
            subBranchInOri = oriDir
            for node in subBranches:
                subBranch = os.path.join(subBranch, node)
                subBranchInOri = os.path.join(subBranchInOri, node)
                self.pathCache.update({subBranchInOri: subBranch})
            return subBranch
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise


    def __lookupSubDirTranslation(self, theDir):
        # try to find a translation for a parent of the current dir
        try:
            return self.pathCache[theDir]
        except (KeyError, AttributeError, NameError):
            try:
                branch = ""
                subDir = theDir
                while (1):
                    try:
                        ##debug_dump("****** LOOKING FOR %s in cache / %d" % (subDir, subDir in self.pathCache))
                        if (subDir == separator or subDir == "") and leaf == "": raise StopIteration
                        tranSubDir = self.pathCache[subDir] # raise AttributeError or go on
                        return self.__addToPathCache(subDir, tranSubDir, branch) # match ! update cache
                    except KeyError:
                        subDir, leaf = os.path.split(subDir)
                        branch = os.path.join(leaf, branch)
                        pass
            except StopIteration:
                raise KeyError
        except (UserCancel, SystemExit):
            raise
        except KeyError:
            raise
        except:
            debug_dump(traceback.format_exc())
            raise


    def translatePath(self, thePath):
        # deals with the fact that the file may be a net mount of a remote host's drive
        # in which case the absolute file path in the remote host's context is not applicable to the present host
        # we do this by searching a common directory between the Library file and the track file
        # and trying to adopt the same root tree of the Library file for finding the track file
        # e.g.: library file in /Volumes/foo/iTunes/iTunes Library.itl
        # track file in /Users/dummy/Music/iTunes/iTunes Media/Music/theArtist/theAlbum/theTrack.mp4
        # tentative translated path: /Volumes/foo/iTunes/iTunes Media/Music/theArtist/theAlbum/theTrack.mp4
        global separator
        try:
            thePath = uriToPath(thePath)
            
            if os.path.exists(thePath):
                return thePath
            dirFilePath, baseFile = os.path.split(thePath)
            return os.path.join(self.__lookupSubDirTranslation(dirFilePath), baseFile) # raises AttributeError/NameError if fails
        except KeyError:
            try:
                libPathSplit = os.path.dirname(self.libFilePath).split(separator)
                dirPathSplit = dirFilePath.split(separator)
                ##debug_dump("**libPathSplit: %s %s" % (repr(libPathSplit), repr(type(libPathSplit))))
                ##debug_dump("**dirPathSplit: %s %s" % (repr(dirPathSplit), repr(type(dirPathSplit))))
                for i in range(1,len(dirPathSplit)): # we start at 1 as the first item is "" (root)
                    branch = separator.join([x for x in dirPathSplit[i+1:]])
                    tree = separator.join([x for x in dirPathSplit[:i+1]])
                    try:
                        # lookup for the first occurence of the dir's name in the "real" path
                        # raise ValueError if not found
                        k = libPathSplit.index(dirPathSplit[i], 0)
                        while k>=0:
                            aspiredTranslation = separator.join([x for x in libPathSplit[:k+1]])
                            ##debug_dump("Iter %d, ori path: %s , translated path: %s" % (i,tree,aspiredTranslation))
                            if os.path.exists(os.path.join(aspiredTranslation, branch, baseFile)):
                                return os.path.join(self.__addToPathCache(tree, aspiredTranslation, branch), baseFile)
                            else:
                                k += libPathSplit[k+1:].index(dirPathSplit[i])
                    except ValueError:
                        pass
            except StopIteration:
                return os.path.join(translatedPath, baseFile)
            
            raise Exception ("Cannot translate directory for file %s." % repr(thePath))
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))
            pass
        except:
            debug_dump(traceback.format_exc())
            pass
    #finally:
    ##debug_dump("CACHE: %s" % repr(self.pathCache))


    
###
### MANAGEMENT OF THE ARTWORK
###


    def __prepareArtworkPath(self, theDirectory):
        try:
            theDirectory = os.path.join(self.artworkPath, theDirectory)
            if not os.path.isdir(theDirectory):
                os.makedirs(theDirectory)
            return theDirectory
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise



    def audioFileHasArtwork(self, audioFilePath):
        try:
            if not os.path.isfile(audioFilePath):
                debug_dump("Unable to read file %s for obtaining artwork" % audioFilePath)
                return True # don't want to spend too much time on it
            audiofile = mutagenFile(audioFilePath) # mutagen can automatically detect format and type of tags
            if audiofile == None:
                return True # don't want to spend too much time on it
            try:
                if audiofile.tags.has_key('covr') or audiofile.tags.has_key('APIC:'):
                    return True
                else: return False
            except:
                return True # don't want to spend too much time on it
        except (UserCancel, SystemExit):
            raise
        except Exception, e:
            debug_dump(to_str(e))
            raise


    def getAudioFileArtwork(self, audioFilePath):
        try:
            dest = os.path.join(self.__prepareArtworkPath('AudioFiles'), os.path.splitext(os.path.basename(audioFilePath))[0])
            if audioFilePath=='' or not os.path.isfile(audioFilePath):
                debug_dump("Unable to read file %s for obtaining artwork" % audioFilePath)
                return ''
            if os.path.isfile(dest) and os.path.getmtime(audioFilePath) < os.path.getmtime(dest):
                # our copy is more recent that the original -> no need to process
                return dest
            audiofile = mutagenFile(audioFilePath) # mutagen can automatically detect format and type of tags
            if audiofile == None:
                #debug_dump("File format not supported for %s. Skipping." % audioFilePath)
                return ''
            try:
                artwork = audiofile.tags['covr'][0] # for M4A
                if artwork.imageformat == artwork.FORMAT_JPEG: ext='.jpg'
                else: ext='.png'
            except:
                try:
                    artwork = audiofile.tags['APIC:'] # for MP3
                    if artwork.mime == 'image/jpeg': ext='.jpg'
                    else: ext='.png'
                    artwork = artwork.data
                except:
                    return ''
                pass
            dest += ext
            with open(dest, 'wb') as img:
                img.write(artwork) # write artwork to new image
            return dest
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise


    def getArtworkPath(self, persistentID):
        # There seems to be an erratic bug when using format() with XBMC's embedded Python version
        # So I'm going for a translation table as below
        int2Str = ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15']
        subDirs = ["Cache", "Cloud", "Download"]
        try:
            self.libArtworkPath
        except:
            self.libArtworkPath = os.path.join(os.path.dirname(self.libFilePath), "Album Artwork")
            self.libArtworkPath = self.translatePath(self.libArtworkPath)
            self.libPersistentID = self.getConfigItem("Library Persistent ID")
        if self.libArtworkPath == None:
            return
        try:
            #fileSubPath = os.path.join(self.libPersistentID,
            #                           "{:02}".format(int(persistentID[-1:],16)),
            #                           "{:02}".format(int(persistentID[-2:-1],16)),
            #                           "{:02}".format(int(persistentID[-3:-2],16)),
            #                           self.libPersistentID + "-" + persistentID + ".itc")
            fileSubPath = os.path.join(self.libPersistentID, int2Str[int(persistentID[-1:],16)],
                                       int2Str[int(persistentID[-2:-1],16)],
                                       int2Str[int(persistentID[-3:-2],16)],
                                       self.libPersistentID + "-" + persistentID + ".itc")
            for subDir in subDirs:
                tentativePath = os.path.join(self.libArtworkPath, subDir, fileSubPath)
                #debug_dump("Trying %s  %s" % (tentativePath, subDir))
                if os.path.exists(tentativePath):
                    # CREATE THE ARTWORK FROM ITC FILE
                    #debug_dump("ITC artwork found for: %s" % repr(persistentID))
                    cacheFileName = itcToPictureFile(tentativePath, self.__prepareArtworkPath(subDir))
                    return cacheFileName
            return None
        except (UserCancel, SystemExit):
            raise
        except:
            debug_dump(traceback.format_exc())
            raise


    

def main():
    dbFileDir = "/Users/arnaud/Desktop"
        #try:
    #dbFile = sys.argv[1]
        #except:
        #print "Usage itunes_parser.py <DBfile>"
    #sys.exit(1)
    try:
        if "--init" in sys.argv:
            db = SyncTunesDB(dbFileDir, "stdb.db", "/Volumes/Rallonge/Shared/iTunes/iTunes Library", reset=True)
            return
        else:
            db = SyncTunesDB(dbFileDir, "stdb.db", "/Volumes/Rallonge/Shared/iTunes/iTunes Library")

            #            print db.translatePath("/toto/tata/iTunes.local/iTunes Media/Music/BeenabarAAA/Beenabar/01 Bon Anniversaire.m4a")
            #print db.translatePath("/toto/tata/iTunes.local/iTunes Media/Music/Unknown Artist/New World Record/6x6.mp3")
            #print db.translatePath("/toto/tata/iTunes.local/iTunes Media/Music/Unknown Artist/New World Record/6x6.mp3")
            #print db.translatePath("/toto/tata/iTunes.local/iTunes Media/Music/Unknown Artist/Unknown Album/A toi.mp3")
                #        print "1 %d" % db.compareRecords({'name': 'abcd','sortname': 'efgh', 'persistent': '12a3'},
                #          {'name': 'abcd','sortname': 'efgh', 'persistent': '23a5'},
        #                {'name', 'sortname'})
                #print "0 %d" % db.compareRecords({'name': 'abcd','sortname': 'efgh', 'persistent': '12a3'},
                #                       {'name': 'abcd','sortname': 'hggf', 'persistent': '23a5'},
        #                       {'name', 'sortname'})
                #print "1 %d" % db.compareRecords({'name': 'abcd','sortname': 'efgh', 'persistent': '12a3'},
                #                       {'name': 'fgg','sortname': 'efgh', 'persistent': '23a5'},
        #                       {'sortname'})
                #print "1 %d" % db.compareRecords({'name': 'abcd','sortname': 'efgh', 'persistent': '12a3'},
                #                       {'sortname': 'efgh'},
        #                       {'sortname'})
                #print "0 %d" % db.compareRecords({'name': 'abcd','sortname': 'efgh', 'persistent': '12a3'},
                #                       {'titi': 'abcd','toto': 'hggf', 'tata': '23a5'},
        #                       ['name', 'sortname'])
        #a=db._insert('artists', {'name':'The Beatles', 'sortname': 'Beatles'})
        #print "insert 1: %d" %a
        #print "*********************************"
        #print "*********************************"
        #a=db._select('artists', {'name':'The Beatles', 'sortname': 'Beatles'})
        #print "select 1: %s" %repr(a)
        #print "*********************************"
        #print "*********************************"
    
        #db.cache['lastartist'] = {'name':'The Beatles', 'sortname': 'Beatles', 'id': 1}
    #a=db.getArtistFromAttributes({'name':'The Beatles', 'sortname': 'Beatles'}, True)
        #a=db.findOrCreateArtist({'name':'The Beatles', 'sortname': 'Beatles'})
        #print "Get artist 1: %s" %repr(a)
        #print "*********************************"
        #a=db.findOrCreateArtist({'name':'The Beatles', 'sortname': 'Bobo'})
        #print "*********************************"
        #a=db.findOrCreateArtist({'name':'The Beatles', 'sortname': 'Bobo'})

    
        #a=db.checkMandatoryAttributes({'name':'The Beatles', 'sortname': 'Bobo'},{'name':str, 'sortname':str, 'persistence':int})
        #print "Check mandatory 1: %d (0)" %a
    
        #a=db.checkMandatoryAttributes({'name':'The Beatles', 'sortname': 'Bobo'},{'name':str, 'sortname':str})
        #print "Check mandatory 2: %d (0)" %a
    
        #a=db.findOrCreateAlbum({'name':'Abbey Road', 'id':'1', 'persistent': 1234, 'artistid': 1, 'albumartistid': 2})
        #print "Find or create album: %s" %a

        #a=db.findOrCreateAlbum({'name':'Abbey Road', 'id':1, 'persistent': 1234, 'artistid': 1, 'albumartistid': 2})
        #print "Find or create album: %s" %a

    #   a=db.getArtistFromAttributes({'name':'The Beegees', 'sortname': 'Beegees'}, False)
    #print "Get artist 2: %s" %repr(a)
    
    #   a=db.getArtistFromAttributes({'name': u'The Beatles', 'sortname': u'Beatles'}, True)
    #   print "Did we just hit the cache ??  %s" % repr(a)

    
    #   a=db.getArtistFromAttributes({'name':'The Beegees'}, True)
    #   print "Get artist 2: %s" %repr(a)

    #   a=db._update('artists', {'name':'The Beegees', 'sortname': 'Beegees'}, {'name':str, 'sortname':str})
    #   print "update 1: %s" %repr(a)

    #   a=db._insert('artists', {'name':'The Beegees', 'sortname': 'Beegees'})
#   print "insert 2: %s" %repr(a)

    #db.addAlbum({'name':'test', 'id':12, 'persistent':'12a3'})
#db.ResetDB()
#iparser = ITunesParser(db.AddTrackNew, db.AddPlaylistNew, db.setConfigItem)
#    try:
#iparser.Parse(xmlfile)
#artwork = artworkProcessor("/Users/arnaud/Desktop",os.path.dirname(xmlfile),db)
#artwork.Process()
    #a = db.getArtists('artist')
    #a = db.getArtists('composer')
    #a = db.getTracksWithRating(1)
    #a=db.getTracksByArtist(3, 'albumartist')
    #a=db.getTracksByArtist(9, 'artist')
    #a=db.getTracksInAlbum(1)
    #
    #    def getTracksInGenre(self, genreid):
        a=db.getArtists()
        a=db.getTracksByArtist(11, 'artist')
        for i in range(1, len(a)):
            b = dict([(x, a[i][x]) for x in a[i].keys()])
            print repr(b)


    except:
        debug_dump(traceback.format_exc())


if __name__=="__main__":
    #profile_main()
    main()

