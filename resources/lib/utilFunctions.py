#!
# -*- coding: utf-8 -*-
# module for utility functions

import traceback
import sys
from urllib2 import url2pathname
try:
    import xbmc
    xbmcloaded = True
except:
    import logging
#logging.basicConfig(filename="/tmp/tutu.log",format='%(asctime)-15s %(message)s', level=logging.DEBUG)
    logging.basicConfig(format='%(asctime)-15s %(message)s', level=logging.DEBUG)
    xbmcloaded = False

global endianPrefix
global debug_flag

debug_flag = True
indent_count = 0

if sys.byteorder == "little":
    endianPrefix = ">" # Intel or ARM (likely)
else:
    endianPrefix = "<"

def set_debugFlag(value):
    global debug_flag
    debug_flag = value

class UserCancel(Exception):
    pass

def debug_dump(message, indent=0):
    global debug_flag
    global indent_count
    global logRoutine
    try:
        if message=="artworkpath": raise Exception("BINGO")
        if debug_flag:
            if indent<0:
                indent_count = indent_count + indent
            message= "SyncTunes " + message.rjust(len(message)+indent_count)
            if indent>0:
                indent_count = indent_count + indent
            try:
                xbmc.log(msg=message, level=xbmc.LOGNOTICE)
            except:
                if not xbmcloaded:
                    logging.debug(message)
                pass
    except Exception,e:
        print traceback.print_exc()
        pass

def uriToPath(theURI):
    pathPrefix = "file://localhost"
    if theURI[0:len(pathPrefix)] == pathPrefix:
        return unicode(url2pathname(str(theURI))[len(pathPrefix):],sys.getfilesystemencoding())
    else:
        return theURI

def to_unicode(text):
    if (isinstance(text, unicode)):
        return text
    if (hasattr(text, '__unicode__')):
        return text.__unicode__()
    text = str(text)
    try:
        return unicode(text, 'utf-8')
    except UnicodeError:
        pass
    try:
        return unicode(text, locale.getpreferredencoding())
    except UnicodeError:
        pass
    return unicode(text, 'latin1')

def to_str(text):
    if (isinstance(text, str)):
        return text
    return format(str(text.args[0])).encode("utf-8")
        #if (hasattr(text, '__unicode__')):
#text = text.__unicode__()
        #    if (hasattr(text, '__str__')):
#text = text.__str__()
#    return text.encode('utf-8')
