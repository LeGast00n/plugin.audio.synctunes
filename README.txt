SyncTunes for XBMC
==================
This plugin imports an iTunes library into XBMC. After importing, you will
see categories that correspond with their iTunes counterparts.

* Artists
* Album Artists
* Genres
* Albums
* Ratings
* Playlists

Configuration
=============
The plugin needs to know where your 'iTunes Music Library[.itl]' file is. If 
you haven't explicitly pointed iTunes to a non-standard library location, 
the default of "~/Music/iTunes/iTunes Music Library[.itl]" should work fine. 
Otherwise, please enter in the correct path in the plugin's settings dialog.

If you select "Auto update library", the plugin will compare the modification
time of your iTunes library with its current database and update the database
automatically on start. This is disabled by default.

Translations
============
If you'd like to help translate this plugin to another language, please send
a patch to la at iscla dot net.

If possible, patch against the most recent version at:

  https://bitbucket.org/LeGast00n/plugin.audio.synctunes


Known Issues
============
Please log issues at https://bitbucket.org/LeGast00n/plugin.audio.synctunes

Credits
=======
LAI (current owner)
Anoop Menon (for showing the example)
AlfredJKwack (for following it)
joe.walt…@gmail.com (titl project)
the Crypto team
Michael Urman (& mutagen team)
Frost (addonscan)
