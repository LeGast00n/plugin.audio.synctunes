"""
	SyncTunes: plugin for synchronizing with iTunes library
    """

__plugin__ = "SyncTunes"
__author__  = "LAI info at iscla net"
__credits__ = "AlfredJKwack, Anoop Menon, jingai"
__url__     = "https://bitbucket.org/LeGast00n/plugin.audio.synctunes"

__addon__ = "plugin.audio.synctunes"

__dbFileName__ = 'stdb.db'
__platform__ = 'osx'

import traceback

import sys
import time
import os
import os.path
import urllib2

import xbmc
import xbmcgui as gui
import xbmcplugin as plugin
import xbmcaddon

from resources.lib.utilFunctions import *

from time import sleep
from traceback import print_exc


debug_dump("SyncTunes initializing...")

global libFile


progressDlg = None
addon = xbmcaddon.Addon(id=__addon__)
BASE_URL = "%s" % (sys.argv[0])
PLUGIN_PATH = addon.getAddonInfo('path')
USER_ADDONDATA_PATH = xbmc.translatePath(addon.getAddonInfo('profile'))
RESOURCE_PATH = os.path.join(PLUGIN_PATH, 'resources')
ICONS_PATH = os.path.join(RESOURCE_PATH, 'icons')
LIB_PATH = os.path.join(RESOURCE_PATH, 'lib')
sys.path.append(LIB_PATH)

# we need to put these imports AFTER the sys.path.append has taken place as there are some cascade dependencies
try:
    from DialogAddonScan import AddonScan, xbmcguiWindowError
    from resources.lib.SyncTunesDB import *
    from resources.lib.ITLFileParser import *
except Exception, e:
    debug_dump(to_str(e))
    exit(1)

try:
    libFile = addon.getSetting('itunes_library_path')
except:
    pass
if (libFile == ""):
    try:
        libFile = os.getenv('HOME') + '/Music/iTunes/iTunes Library'
        if not os.path.isfile(libFile):
            libFile += '.itl'
        addon.setSetting('itunes_library_path', libFile)
    except:
        pass
try:
    DB_FILE_PATH = os.path.join(USER_ADDONDATA_PATH, __dbFileName__)
    stdb = SyncTunesDB(DB_FILE_PATH, libFile, addon)
except Exception,e:
    debug_dump(to_str(e))
    raise
debug_dump("SyncTunes plugin initialized")
debug_dump("Python version %s." % repr(sys.version))

def listTracks(params):
    global stdb
    try:
        if params.has_key('albumid'):
            tracks = stdb.getTracksInAlbum(params['albumid'])
        elif params.has_key('artistid'):
            tracks = stdb.getTracksByArtist(params['artistid'])
        elif params.has_key('rating'):
            tracks = stdb.getTracksWithRating(params['rating'])
        elif params.has_key('genreid'):
            tracks = stdb.getTracksInGenre(params['genreid'])
        elif params.has_key('playlistid'):
            tracks = stdb.getTracksInPlaylist(params['playlistid'])
        else:
            tracks = stdb.getTracks()
    except Exception, e:
        debug_dump(str(e))
        raise

    try:
        trackLabels = ['artist', 'album', 'title', 'genre', 'tracknumber', 'duration', 'year', 'rating', 'playcount']
        
        plugin.setContent(handle = int(sys.argv[1]), content= 'songs')
        cnt = 1
        for track in tracks:
            if track['artworkpath'] <> '':
                item = gui.ListItem(track['title'], thumbnailImage=track['artworkpath'])
            else:
                item = gui.ListItem(track['title'])
            labels = dict((x, track[x]) for x in trackLabels)
            
            labels['duration'] = int(labels['duration'])/1000.0
            labels['rating'] = str(labels['rating'])
            labels['count'] = cnt
            cnt += 1
            item.setInfo(type='music', infoLabels=labels)
            fileName = stdb.translatePath(track['filename'])
            plugin.addDirectoryItem(handle = int(sys.argv[1]), url=fileName, listitem = item, isFolder = False)

        plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_TRACKNUM)
        plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_UNSORTED)
        if not params.has_key('genreid'):
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_GENRE)
        plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_TITLE)
        plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_TITLE_IGNORE_THE)
        if not params.has_key('rating'):
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_SONG_RATING)
        #    plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_YEAR)
        if not params.has_key('artistid'):
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_ARTIST)
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_ARTIST_IGNORE_THE)
        if not params.has_key('albumid'):
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_ALBUM_IGNORE_THE)
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_ALBUM)
        if params.has_key('playlistid'):
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_PLAYLIST_ORDER)
        return
    except (UserCancel, SystemExit):
        raise
    except:
        debug_dump(traceback.format_exc())
        pass

def listAlbums(params):
    global stdb, ICONS_PATH
    if params.has_key('albumid'):
        listTracks(params)
        return
    elif params.has_key('artistid'):
        # if artistid is specified, list only the albums for this artist
        albums = stdb.getAlbumsByArtistId(params['artistid'])
    else:
        albums = stdb.getAlbums()
    if albums == []:
        return
    try:
        defaultIcon = ICONS_PATH+'/album.png'
        plugin.setContent(handle = int(sys.argv[1]), content= 'albums') 
        artistSort = False
        
        if len(albums) > 0:
            for theAlbum in albums:
                icon = stdb.translatePath(theAlbum['artworkpath'])
                if icon == None or icon == "": icon = defaultIcon
                theTitle = theAlbum['name']
                theLabels = {'album': theAlbum['name'], 'rating': str(theAlbum['rating'])}
                if (theAlbum['artist']):
                    artistSort = True
                    theLabels.update({'artist': theAlbum['artist']})
                    theTitle += " (" + addon.getLocalizedString(30301) + " " + theAlbum['artist'] + ")"
                item = gui.ListItem(theTitle , icon, thumbnailImage=icon)
                item.setInfo(type='music', infoLabels = theLabels)
                plugin.addDirectoryItem(handle = int(sys.argv[1]),
                                        url=BASE_URL+'?action=albums&albumid=%d' % (theAlbum['id']),
                                        listitem = item,
                                        isFolder = True)
            
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_UNSORTED)
            plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_ALBUM)    
            if artistSort:
                plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_ARTIST)
        else:
            listTracks(params)
    except (UserCancel, SystemExit):
        raise
    except:
        debug_dump(traceback.format_exc())
        pass


def listArtists(params):
    #Type param can be 'artist', 'albumartist', 'composer'
    global stdb,ICONS_PATH
    if params.has_key('artistid'):
        if params['type'] == 'artist':
            listTracks(params)
        else:
            listAlbums(params)
        return
    try:
        artistType = params['type']
    except:
        artistType = 'artist'
    try:
        artists = stdb.getArtists(artistType)
        icon = ICONS_PATH+'/artist.png'
        plugin.setContent(handle = int(sys.argv[1]), content= 'artists') 
        for artist in artists:
            item = gui.ListItem(artist['name'], thumbnailImage=icon)
            #item.setInfo(type='Music', infoLabels={ "Artist": artist['name'] })
            plugin.addDirectoryItem(handle = int(sys.argv[1]),
                                    url=BASE_URL+'?action=artists&artistid=%d&type=%s' % (artist['id'], artistType),
                                    listitem = item,
                                    isFolder = True)
        plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_LABEL)
        plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_UNSORTED)
    except (UserCancel, SystemExit):
        raise
    except:
        debug_dump(traceback.format_exc())
        pass


def listPlaylists(params):
    global stdb, ICONS_PATH
    try:
        params['playlistid']
        listTracks(params)
        return
    except (UserCancel, SystemExit):
        raise
    except:
        pass
    playlists = stdb.GetPlaylists()
    icon = ICONS_PATH+'/playlist.png'
    plugin.setContent(handle = int(sys.argv[1]), content='albums')
    for (playlistid, playlist) in playlists:
        item = gui.ListItem(playlist, thumbnailImage=icon)
        # item.setInfo(type='Music')
        plugin.addDirectoryItem(handle = int(sys.argv[1]),
                                url=BASE_URL+'?action=playlists&playlistid=%d' % playlistid,
                                listitem = item,
                                isFolder = True)
    plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_LABEL)
    plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_UNSORTED)
    return


def listGenres(params):
    global stdb, BASE_URL, ICONS_PATH, db
    try:
        params['genreid']
        listTracks(params)
        return
    except (UserCancel, SystemExit):
        raise
    except:
        pass
    genres = stdb.getGenres()
    if not genres:
        return
    icon = ICONS_PATH+'/genres.png'
    for genre in genres:
        item = gui.ListItem(genre['name'], thumbnailImage=icon)
        plugin.addDirectoryItem(handle = int(sys.argv[1]),
                                url=BASE_URL+'?action=genres&genreid=%d' % (genre['id']),
                                listitem = item,
                                isFolder = True, totalItems=100)
    plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_LABEL)
    plugin.addSortMethod(handle=int(sys.argv[ 1 ]), sortMethod=plugin.SORT_METHOD_UNSORTED)    
    return



def listRatings(params):
    global BASE_URL,ICONS_PATH
    try:
        params['rating']
        listTracks(params)
        return
    except (UserCancel, SystemExit):
        raise
    except:
        pass
    for a in range(1,6):
        if (a<2):
            thestr = addon.getLocalizedString(30200)
        else:
            thestr = addon.getLocalizedString(30201)    
        rating = thestr%a
        item = gui.ListItem(rating, thumbnailImage=ICONS_PATH+'/star%d.png' % a)
        plugin.addDirectoryItem(handle = int(sys.argv[1]),
                                url=BASE_URL+'?action=ratings&rating=%d' % a,
                                listitem = item,
                                isFolder = True)


def viewProgress(current=0, max=0):
    global progressDlg
    firstTime = False
    try:
        #debug_dump("progressDlg %s" % repr(progressDlg))
        progressDlg
        if progressDlg == None: raise Exception('')
    except:
        firstTime = True
        debug_dump("Instantiating AddonScan as nonexistent or equal to None")
        #progressDlg = gui.DialogProgress()
        progressDlg = AddonScan()
        try:
            progressDlg.create("SyncTunes import...")
        except:
            debug_dump("Create of AddonScan failed")
            debug_dump(traceback.format_exc())
            exit(1)
    try:
        if current == max == 0:
            if not firstTime:
                return
            else:
                max = 100
        if progressDlg.iscanceled():
            raise UserCancel
        pct = int(100*float(current)/float(max))
#debug_dump("Calling update(%d) on AddonScan" % pct)
        progressDlg.update(percent1=pct, line2="Progress: [B]%i%%[/B]" % pct)

    except (UserCancel, SystemExit):
        raise
    except xbmcguiWindowError:
        debug_dump(traceback.format_exc())
    except:
        debug_dump(traceback.format_exc())



def hideProgress():
    global progressDlg
    debug_dump("Want to close any progress dialog")
    try:
        progressDlg
    except:
        return
    else:
        progressDlg.close()
        #del progressDlg


def syncLibrary(callBackRoutine = None, forceUpdate = False):
    global stdb, libFile, DB_FILE_PATH
    try:
        debug_dump("Entering syncLibrary()")
        #        stdb.ResetDB()
        if not forceUpdate:
            libModifTime = os.path.getmtime(libFile)
            libModifTimeInDB = stdb.getConfigItem("library_mdate")
            debug_dump("Auto update check lib modified date: %s / DB uploaded on: %s" % (repr(libModifTime),repr(libModifTimeInDB)))
            
            if libModifTimeInDB <> None and float(libModifTimeInDB) == libModifTime:
                debug_dump("DB up to date, no need to resync")
                return
        ITLFileParserPath = os.path.join(os.path.dirname(__file__), 'resources', 'lib', 'ITLFileParser.py')
        debug_dump("Launching %s" % ITLFileParserPath)
        xbmc.executebuiltin('XBMC.RunScript("%s", "--lib", "%s", "--db", "%s")' % (ITLFileParserPath, libFile, DB_FILE_PATH))
        stdb.setConfigItem("library_mdate", str(libModifTime))
        return
    
        newpid = 0 #os.fork()
        #if newpid <= 0: debug_dump("Cannot fork resync process")
        #elif newpid == 0: debug_dump("Resync process forked successfully")
        if newpid <= 0:
            # the resync is carried out by the child process (or the main process if fork() failed)
            theParser = ITLFileParser(libFile, stdb, addon, callBackRoutine)
            theParser.doParse()
            stdb.setConfigItem("library_mdate", str(libModifTime))
            hideProgress()
            debug_dump("Finishing syncLibrary()")
            if newpid == 0: sys.exit(0)
    except UserCancel:
        pass
    except SystemExit:
        raise
    except:
        debug_dump(traceback.format_exc())


def getParams(paramstring):
    params = {}
    paramstring = str(paramstring).strip()
    paramstring = paramstring.lstrip('?')
    if not paramstring:
        return params
    paramlist = paramstring.split('&')
    for param in paramlist:
        (k,v) = param.split('=')
        params[k] = v
    return params

def processParams(params):
    try:
        action = params['action']
    except:
        return root_directory()
    debug_dump("Processing params: %s" % repr(params))
    
    if action == 'artists':
        return listArtists(params)
    elif action == 'albums':
        return listAlbums(params)
    elif action == 'playlists':
        return listPlaylists(params)
    elif action == 'tracks':
        return listTracks(params)
    elif action == 'genres':
        return listGenres(params)
    elif action == 'ratings':
        return listRatings(params)
    elif action == 'rescan':
        root_directory()
        plugin.endOfDirectory(handle = int(sys.argv[1]), succeeded = True, updateListing = True)
        syncLibrary(callBackRoutine = viewProgress, forceUpdate = True)
        #        plugin.endOfDirectory(handle = int(sys.argv[1]), succeeded = True, updateListing = True)
        sys.exit(0)
    
    root_directory()


def root_directory(hideImportLib=False):
    global ICONS_PATH
    # add the artists entry
    item = gui.ListItem(addon.getLocalizedString(30400), thumbnailImage=ICONS_PATH+'/artist.png')
    item.setInfo(type='Music', infoLabels={ 'Title': addon.getLocalizedString(30400) })
    plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=artists', listitem = item,
                            isFolder = True)
    
    item = gui.ListItem(addon.getLocalizedString(30401), thumbnailImage=ICONS_PATH+'/artist.png')
    item.setInfo(type='Music', infoLabels={ 'Title': addon.getLocalizedString(30401) })
    plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=artists&type=albumartist', listitem = item,
                            isFolder = True)
    
    item = gui.ListItem(addon.getLocalizedString(30402), thumbnailImage=ICONS_PATH+'/albums.png')
    item.setInfo(type='Music', infoLabels={ 'Title': addon.getLocalizedString(30402) })
    plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=albums', listitem = item, 
                            isFolder = True)
    
    item = gui.ListItem(addon.getLocalizedString(30403), thumbnailImage=ICONS_PATH+'/playlist.png')
    item.setInfo(type='Music', infoLabels={ 'Title': addon.getLocalizedString(30403) })
    plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=playlists', listitem = item, 
                            isFolder = True)
    
    item = gui.ListItem(addon.getLocalizedString(30404), thumbnailImage=ICONS_PATH+'/genres.png')
    item.setInfo(type='Music', infoLabels={ 'Title': addon.getLocalizedString(30404) })
    plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=genres', listitem = item, 
                            isFolder = True)
    
    item = gui.ListItem(addon.getLocalizedString(30405), thumbnailImage=ICONS_PATH+'/star.png')
    item.setInfo(type='Music', infoLabels={ 'Title': addon.getLocalizedString(30405) })
    plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=ratings', listitem = item,
                            isFolder = True)
    
    if not hideImportLib:
        hide_import_lib = addon.getSetting('hide_import_lib')
        if (hide_import_lib == ''):
            addon.setSetting('hide_import_lib', 'false')
            hide_import_lib = 'false'
        if (hide_import_lib == 'false'):
            item = gui.ListItem(addon.getLocalizedString(30406), thumbnailImage=ICONS_PATH+'/import.png')
            plugin.addDirectoryItem(handle = int(sys.argv[1]), url=BASE_URL+'?action=rescan', listitem = item, 
                                    isFolder = True, totalItems=100)

try:
    debug_dump('__name__ = %s' % repr(__name__))
except:
    pass

if __name__=='__main__':
    #global libFile, stdb
    try:
        # auto-update if so configured
        auto_update_lib = addon.getSetting('auto_update')
        if (auto_update_lib == ''): # set default
            debug_dump("Initializing auto_update setting to default")
            addon.setSetting('auto_update', 'false')
            auto_update_lib = 'false'
        if (auto_update_lib == 'true'):
            syncLibrary(forceUpdate = False)
        else:
            debug_dump("Auto update disabled")

        #debug_dump("Processing user action")
        params = sys.argv[2]
        processParams(getParams(params))
        plugin.endOfDirectory(handle = int(sys.argv[1]), succeeded = True)
        #debug_dump("End of user action processing")
    except (UserCancel, SystemExit):
        raise
    except Exception, e:
        print str(e)
        debug_dump(traceback.print_exc())
        print traceback.print_exc()

